package server

// URLPaths contains all paths required for server.
type URLPaths struct {
	// WebResourceCategoryListV1Path - ALL web resource categories.
	webResourceCategoryListV1Path string
	webResourceCategoryV1Path     string

	// Web Resources.
	webResourceListV1Path string
	webResourceV1Path     string

	// Web Resource Category Web Resources.
	webResourceCategoryWebResourceListV1Path string
	webResourceCategoryWebResourceV1Path     string

	// Access Groups.
	accessGroupListV1Path                      string
	accessGroupV1Path                          string
	accessGroupV1WebResourceCategoryListV1Path string
	accessGroupV1ExternalGroupListV1Path       string

	// External Users and Groups' sources.
	externalUsersGroupsSourceListV1Path string
	externalUsersGroupsSourceV1Path     string

	// External Sessions' sources.
	externalSessionsSourceListV1Path string
	externalSessionsSourceV1Path     string

	// External Users.
	externalUserListV1Path                string
	externalUserV1Path                    string
	externalUserV1ExternalGroupListV1Path string

	// External Group Lists.
	externalGroupListV1Path             string
	externalGroupV1Path                 string
	externalGroupExternalUserListV1Path string

	// External Sessions.
	externalUserSessionListV1Path string
	externalUserSessionV1Path     string

	// Health check.
	healthCheckPath string
}

// newDefaultURLPaths returns default URLPaths.
func newDefaultURLPaths(prefix string) URLPaths { // nolint: funlen
	var (
		// WebResourceCategoryListV1Path - ALL web resource categories.
		webResourceCategoryListV1Path = prefix + V1Suffix + "/WebResourceCategories"
		webResourceCategoryV1Path     = prefix + V1Suffix + "/WebResourceCategories/:" + webResourceCategoryNamePathParam

		// Web Resources.
		webResourceListV1Path = prefix + V1Suffix + "/WebResources"
		webResourceV1Path     = webResourceListV1Path + "/:" + webResourceNamePathParam

		// Web Resource Category Web Resources.
		webResourceCategoryWebResourceListV1Path = webResourceCategoryV1Path + "/WebResources"
		webResourceCategoryWebResourceV1Path     = webResourceCategoryWebResourceListV1Path + "/:" + webResourceNamePathParam

		// Access Groups.
		accessGroupListV1Path = prefix + V1Suffix + "/AccessGroups"
		accessGroupV1Path     = accessGroupListV1Path + "/:" + accessGroupNamePathParam

		accessGroupV1WebResourceCategoryListV1Path = accessGroupV1Path + "/WebResourceCategories"
		accessGroupV1ExternalGroupListV1Path       = accessGroupV1Path + "/ExternalGroups"

		// External Users and Groups' sources.
		externalUsersGroupsSourceListV1Path = prefix + V1Suffix + "/ExternalUsersGroupsSources"
		externalUsersGroupsSourceV1Path     = prefix + V1Suffix + "/ExternalUsersGroupsSources/:" +
			externalUsersGroupsSourceNamePathParam

		// External Sessions' sources.
		externalSessionsSourceListV1Path = prefix + V1Suffix + "/ExternalSessionsSources"
		externalSessionsSourceV1Path     = externalSessionsSourceListV1Path + "/:" + externalSessionsSourceNamePathParam

		// External Users.
		externalUserListV1Path = externalUsersGroupsSourceV1Path + "/ExternalUsers"
		externalUserV1Path     = externalUserListV1Path + "/:" + externalUserNamePathParam

		externalUserV1ExternalGroupListV1Path = externalUserV1Path + "/ExternalGroups"

		// External Group Lists.
		externalGroupListV1Path = externalUsersGroupsSourceV1Path + "/ExternalGroups"
		externalGroupV1Path     = externalGroupListV1Path + "/:" + externalGroupNamePathParam

		externalGroupExternalUserListV1Path = externalGroupV1Path + "/ExternalUsers"

		// External Sessions.
		externalUserSessionListV1Path = externalSessionsSourceV1Path + "/ExternalUserSessions"
		externalUserSessionV1Path     = externalUserSessionListV1Path + "/:" + externalUserSessionNamePathParam

		// Health check.
		healthCheckPath = "/healthz"
	)

	return URLPaths{
		webResourceCategoryListV1Path: webResourceCategoryListV1Path,
		webResourceCategoryV1Path:     webResourceCategoryV1Path,

		webResourceListV1Path: webResourceListV1Path,
		webResourceV1Path:     webResourceV1Path,

		webResourceCategoryWebResourceListV1Path: webResourceCategoryWebResourceListV1Path,
		webResourceCategoryWebResourceV1Path:     webResourceCategoryWebResourceV1Path,

		accessGroupListV1Path:                      accessGroupListV1Path,
		accessGroupV1Path:                          accessGroupV1Path,
		accessGroupV1ExternalGroupListV1Path:       accessGroupV1ExternalGroupListV1Path,
		accessGroupV1WebResourceCategoryListV1Path: accessGroupV1WebResourceCategoryListV1Path,

		externalUsersGroupsSourceListV1Path: externalUsersGroupsSourceListV1Path,
		externalUsersGroupsSourceV1Path:     externalUsersGroupsSourceV1Path,

		externalSessionsSourceListV1Path: externalSessionsSourceListV1Path,
		externalSessionsSourceV1Path:     externalSessionsSourceV1Path,

		externalUserListV1Path:                externalUserListV1Path,
		externalUserV1Path:                    externalUserV1Path,
		externalUserV1ExternalGroupListV1Path: externalUserV1ExternalGroupListV1Path,

		externalGroupListV1Path: externalGroupListV1Path,
		externalGroupV1Path:     externalGroupV1Path,

		externalUserSessionListV1Path:       externalUserSessionListV1Path,
		externalUserSessionV1Path:           externalUserSessionV1Path,
		externalGroupExternalUserListV1Path: externalGroupExternalUserListV1Path,

		healthCheckPath: healthCheckPath,
	}
}
