package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostExternalUserSessionListV1(stor ExternalUserSessionV1sOpener) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse query parameters.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Get External Sessions Source Name.
		extSessionsSourceName, err := parseExternalSessionsSourceName(c)
		if err != nil {
			return err
		}

		// Get payload.
		var sessions []storage.ExternalUserSessionV1

		if err := c.Bind(&sessions); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Validate.
		for i := range sessions {
			if sessions[i].Metadata.ExternalSource.SourceName != extSessionsSourceName {
				return ErrDifferentSources{
					expected: extSessionsSourceName,
					given:    sessions[i].Metadata.ExternalSource.SourceName,
					resource: sessions[i],
				}.HTTPErr()
			}
		}

		// Create.
		newSessions, err := stor.OpenExternalUserSessionV1sContext(
			c.Request().Context(), sessions, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, newSessions)
	}
}
