package storage

import (
	"encoding/json"
	"fmt"
	"net"
	"net/url"
)

const (
	WebResourceKind       ResourceKindT = "WebResource"
	WebResourceFilterKind ResourceKindT = "WebResourceFilter"
)

// intWebResourceDataV1 - used to serialize-deserialize WebResourceDataV1.
type intWebResourceDataV1 struct {
	Description WebResourceDescriptionT `json:"description,omitempty" yaml:"description,omitempty"`

	WebResourceCategoryNames []ResourceNameT `json:"webResourceCategoryNames,omitempty" yaml:"webResourceCategoryNames,omitempty"`

	Domains []WebResourceDomainT `json:"domains,omitempty" yaml:"domains,omitempty"`
	URLs    []string             `json:"urLs,omitempty" yaml:"urLs,omitempty"`
	IPs     []string             `json:"iPs,omitempty" yaml:"iPs,omitempty"`
}

// WebResourceDataV1 - data of one WebResourceV1.
type WebResourceDataV1 struct {
	Description WebResourceDescriptionT `json:"description,omitempty" yaml:"description,omitempty"`

	WebResourceCategoryNames []ResourceNameT `faker:"web_resource_web_resource_categories" json:"webResourceCategoryNames,omitempty" yaml:"webResourceCategoryNames,omitempty"`

	Domains []WebResourceDomainT `faker:"web_resource_domain_names" json:"domains,omitempty" yaml:"domains,omitempty"`
	URLs    []url.URL            `faker:"web_resource_urls" json:"urLs,omitempty" yaml:"urLs,omitempty"`
	IPs     []net.IP             `faker:"web_resource_ips" json:"iPs,omitempty" yaml:"iPs,omitempty"`
}

func (wrd *WebResourceDataV1) String() string {
	return fmt.Sprintf("%+v", *wrd)
}

// UnmarshalJSON - json.Unmarshaler interface.
func (wrd *WebResourceDataV1) UnmarshalJSON(b []byte) error {
	f := intWebResourceDataV1{}

	if err := json.Unmarshal(b, &f); err != nil {
		return err
	}

	// URLs
	urls := make([]url.URL, len(f.URLs))

	for i := range f.URLs {
		u, err := url.Parse(f.URLs[i])
		if err != nil {
			return err
		}

		urls[i] = *u
	}

	// IPs
	ips := make([]net.IP, len(f.IPs))
	for i := range f.IPs {
		ips[i] = net.ParseIP(f.IPs[i])
	}

	wrd.Description = f.Description
	wrd.WebResourceCategoryNames = f.WebResourceCategoryNames
	wrd.Domains = f.Domains
	wrd.URLs = urls
	wrd.IPs = ips

	return nil
}

// MarshalJSON - json.Marshaler interface.
func (wrd *WebResourceDataV1) MarshalJSON() ([]byte, error) {
	// URLs
	URLs := make([]string, len(wrd.URLs))
	for i := range wrd.URLs {
		URLs[i] = wrd.URLs[i].String()
	}

	// IPs
	IPs := make([]string, len(wrd.IPs))
	for i := range wrd.IPs {
		IPs[i] = wrd.IPs[i].String()
	}

	f := intWebResourceDataV1{
		Description:              wrd.Description,
		WebResourceCategoryNames: wrd.WebResourceCategoryNames,
		Domains:                  wrd.Domains,
		URLs:                     URLs,
		IPs:                      IPs,
	}

	return json.Marshal(f)
}

// Equal compares data for equality.
func (wrd *WebResourceDataV1) Equal(w *WebResourceDataV1) bool {
	// Description.
	if wrd.Description != w.Description {
		return false
	}

	// WebResourceCategoryName.
	if !CompareResourceNameSets(wrd.WebResourceCategoryNames, w.WebResourceCategoryNames) {
		return false
	}

	// Domains.
	for _, lD := range wrd.Domains {
		var found bool

		for i := range w.Domains {
			if w.Domains[i] == lD {
				found = true

				break
			}
		}

		if !found {
			return false
		}
	}

	for _, rD := range w.Domains {
		var found bool

		for i := range wrd.Domains {
			if rD == wrd.Domains[i] {
				found = true

				break
			}
		}

		if !found {
			return false
		}
	}

	// URLs.
	for _, lU := range wrd.URLs {
		var found bool

		for i := range w.URLs {
			if w.URLs[i] == lU {
				found = true

				break
			}
		}

		if !found {
			return false
		}
	}

	for _, rU := range w.URLs {
		var found bool

		for i := range wrd.URLs {
			if rU == wrd.URLs[i] {
				found = true

				break
			}
		}

		if !found {
			return false
		}
	}

	// IPs.
	for _, lIP := range wrd.IPs {
		var found bool

		for i := range w.IPs {
			if compareIPs(w.IPs[i], lIP) {
				found = true

				break
			}
		}

		if !found {
			return false
		}
	}

	for _, rD := range w.Domains {
		var found bool

		for i := range wrd.Domains {
			if rD == wrd.Domains[i] {
				found = true

				break
			}
		}

		if !found {
			return false
		}
	}

	return true
}

// WebResourceV1 - one WebResourceV1.
type WebResourceV1 struct {
	Metadata MetadataV1        `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Data     WebResourceDataV1 `json:"data,omitempty" yaml:"data,omitempty"`
}

// Equal compares for equality.
func (wrv1 *WebResourceV1) Equal(v *WebResourceV1) bool {
	if !wrv1.Metadata.Equal(v.Metadata) {
		return false
	}

	return wrv1.Data.Equal(&v.Data)
}

func (wrv1 *WebResourceV1) String() string {
	return fmt.Sprintf("%+v", *wrv1)
}

// NewWebResourceV1 - creates a new WebResourceV1.
func NewWebResourceV1(name ResourceNameT) *WebResourceV1 {
	return &WebResourceV1{
		Metadata: NewMetadataV1(WebResourceKind, name, nil),
	}
}

// GetMetadata - returns copy of WebResourceV1 MetadataV1.
func (wrv1 *WebResourceV1) GetMetadata() MetadataV1 {
	return wrv1.Metadata.Copy()
}

// WebResourceFilterV1 - one WebResourceFilterV1 data.
// Conditions are ANDed.
type WebResourceFilterV1 struct {
	NameSubstringMatch        ResourceNameT `json:"nameSubstringMatch,omitempty" yaml:"nameSubstringMatch,omitempty"`
	DescriptionSubstringMatch string        `json:"descriptionSubstringMatch,omitempty" yaml:"descriptionSubstringMatch,omitempty"`

	Domains []WebResourceDomainT `json:"domains,omitempty" yaml:"domains,omitempty"`
	URLs    []url.URL            `json:"urLs,omitempty" yaml:"urLs,omitempty"`
	IPs     []net.IP             `json:"iPs,omitempty" yaml:"iPs,omitempty"`
}

func (v *WebResourceFilterV1) String() string {
	return fmt.Sprintf("%+v", *v)
}
