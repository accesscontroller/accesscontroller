package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostAccessGroupListV1(stor AccessGroupV1sCreator) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse parameters.
		qP, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Parse Access Group List.
		var payload []storage.AccessGroupV1

		if err := c.Bind(&payload); err != nil {
			return err
		}

		// Create a new Access Groups from list.
		created, err := stor.CreateAccessGroupV1sContext(
			c.Request().Context(), payload, qP.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, created)
	}
}
