package server

import (
	"fmt"
	"strconv"

	"github.com/labstack/echo/v4"
)

const (
	IncludeClosedSessions string = "include_closed"
	GetOperationResult    string = "get_operation_result"
)

var (
	ErrParseIncludeClosedSessions = fmt.Errorf(
		"incorrect %q parameter value", IncludeClosedSessions)
	ErrParseGetOperationResult = fmt.Errorf(
		"incorrect %q parameter value", GetOperationResult)
)

type QueryParameters struct {
	IncludeClosedSessions bool
	GetOperationResult    bool
}

// parseQueryParameters parses context query parameters;
// returns *echo.HTTPError on error.
func parseQueryParameters(c echo.Context) (*QueryParameters, error) {
	result := &QueryParameters{}

	// Include closed sessions.
	if incClRaw := c.QueryParam(IncludeClosedSessions); len(incClRaw) != 0 {
		var err error

		result.IncludeClosedSessions, err = strconv.ParseBool(incClRaw)
		if err != nil {
			return nil, echo.ErrBadRequest.SetInternal(ErrParseIncludeClosedSessions)
		}
	}

	if gOpResRaw := c.QueryParam(GetOperationResult); len(gOpResRaw) != 0 {
		var err error

		result.GetOperationResult, err = strconv.ParseBool(gOpResRaw)
		if err != nil {
			return nil, echo.ErrBadRequest.SetInternal(ErrParseGetOperationResult)
		}
	}

	return result, nil
}
