package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func createGetExternalSessionsSourceV1(stor ExternalSessionsSourceV1Getter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get sessions source name.
		name, err := parseExternalSessionsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get.
		source, err := stor.GetExternalSessionsSourceV1Context(
			c.Request().Context(), name)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, source)
	}
}
