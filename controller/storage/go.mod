module gitlab.com/accesscontroller/accesscontroller/controller/storage

go 1.12

require (
	github.com/bxcodec/faker/v3 v3.5.0
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/stretchr/testify v1.6.1
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
