package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func createGetExternalUserSessionV1(stor ExternalUserSessionV1Getter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Sessions Source name.
		sourceName, err := parseExternalSessionsSourceName(c)
		if err != nil {
			return err
		}
		// Name.
		eusn, err := parseExternalUserSessionName(c)
		if err != nil {
			return err
		}

		// Get session.
		session, err := stor.GetExternalUserSessionV1Context(
			c.Request().Context(), sourceName, eusn)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, session)
	}
}
