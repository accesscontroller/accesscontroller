package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createGetExternalUserListV1(gt ExternalUserV1sGetter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Read source name.
		sourceName, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Read request.
		var flt []storage.ExternalUserFilterV1

		if err := c.Bind(&flt); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get users data.
		users, err := gt.GetExternalUserV1sContext(c.Request().Context(),
			flt, sourceName)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, users)
	}
}
