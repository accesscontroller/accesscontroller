package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createDeleteExternalUserListV1(stor ExternalUserV1sDeleter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get source name.
		sourceName, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Parse.
		var userList []storage.ExternalUserV1

		if err := c.Bind(&userList); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check Source Name match.
		for i := range userList {
			if sourceName != userList[i].Metadata.ExternalSource.SourceName {
				return NewErrDifferentSources(sourceName,
					userList[i].Metadata.ExternalSource.SourceName,
					userList[i])
			}
		}

		// Delete.
		if err := stor.DeleteExternalUserV1sContext(c.Request().Context(),
			userList); err != nil {
			return parseError(err)
		}

		return c.NoContent(http.StatusNoContent)
	}
}
