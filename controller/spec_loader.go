package main

import (
	"io/ioutil"
	"log"
	"os"
)

func loadAPISpec(path string) ([]byte, error) {
	// Open OpenAPIv3 Spec
	specF, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	defer func() {
		if err := specF.Close(); err != nil {
			log.Fatalln(err)
		}
	}()

	spec, err := ioutil.ReadAll(specF)
	if err != nil {
		return nil, err
	}

	return spec, nil
}
