package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPutAccessGroupExternalGroupListV1(stor AccessGroup2ExternalGroupV1RelationBinder) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse Access Group.
		accessGroup, err := parseAccessGroupName(c)
		if err != nil {
			return err
		}

		var payload storage.AccessGroup2ExternalGroupListRelationsV1

		if err := c.Bind(&payload); err != nil {
			return err
		}

		// Check values.
		if accessGroup != payload.Data.AccessGroup {
			return &echo.HTTPError{
				Code:    http.StatusBadRequest,
				Message: "Path AccessGrup is not equal to AccessGroup2ExternalGroupListRelationsV1#Data#AccessGroup",
			}
		}

		// Bind.
		if err := stor.BindAccessGroup2ExternalGroupV1sContext(c.Request().Context(), &payload); err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, nil)
	}
}
