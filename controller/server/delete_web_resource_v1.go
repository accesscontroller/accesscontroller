package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createDeleteWebResourceV1(stor WebResourceV1Deleter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Name.
		name, err := parseWebResourceName(c)
		if err != nil {
			return err
		}

		// Get payload.
		var wr = &storage.WebResourceV1{}
		if err := c.Bind(wr); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check names match.
		if name != wr.Metadata.Name {
			return NewErrNamesDoNotMatch(name, wr.Metadata.Name, wr).HTTPErr()
		}

		// Delete.
		if err := stor.DeleteWebResourceV1Context(
			c.Request().Context(), wr); err != nil {
			return parseError(err)
		}

		return c.NoContent(http.StatusNoContent)
	}
}
