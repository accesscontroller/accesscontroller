package storage

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/suite"
	"gopkg.in/yaml.v3"
)

type TestDefaultAccessPolicySuite struct {
	suite.Suite
}

func (ts *TestDefaultAccessPolicySuite) TestMarshalJSON() {
	var tests = []*struct {
		name string

		value DefaultAccessPolicyT

		expectedJSON  string
		expectedError error
	}{
		{
			name:          "test Success Undefined",
			value:         DefaultAccessPolicyUndefined,
			expectedError: nil,
			expectedJSON:  `""`,
		},
		{
			name:          "test Success Allow",
			value:         DefaultAccessPolicyAllow,
			expectedError: nil,
			expectedJSON:  `"allow"`,
		},
		{
			name:          "test Success Deny",
			value:         DefaultAccessPolicyDeny,
			expectedError: nil,
			expectedJSON:  `"deny"`,
		},
	}

	for _, test := range tests {
		serialized, gotErr := json.Marshal(test.value)

		if !ts.Equalf(test.expectedError, gotErr, "test %q", test.name) {
			ts.FailNowf("Aborted", "test %q", test.name)
		}

		ts.JSONEqf(test.expectedJSON, string(serialized), "test %q", test.name)
	}
}

func (ts *TestDefaultAccessPolicySuite) TestUnmarshalJSON() {
	var tests = []*struct {
		name string

		JSON string

		expectedValue DefaultAccessPolicyT
		expectedError error
	}{
		{
			name:          "test Success Undefined",
			JSON:          `""`,
			expectedError: nil,
			expectedValue: DefaultAccessPolicyUndefined,
		},
		{
			name:          "test Success Allow",
			JSON:          `"allow"`,
			expectedError: nil,
			expectedValue: DefaultAccessPolicyAllow,
		},
		{
			name:          "test Success Deny",
			JSON:          `"deny"`,
			expectedError: nil,
			expectedValue: DefaultAccessPolicyDeny,
		},
	}

	for _, test := range tests {
		var deserialized DefaultAccessPolicyT

		gotErr := json.Unmarshal([]byte(test.JSON), &deserialized)

		if !ts.Equalf(test.expectedError, gotErr, "test %q", test.name) {
			ts.FailNowf("Aborted", "test %q", test.name)
		}

		ts.Equalf(test.expectedValue, deserialized, "test %q", test.name)
	}
}

func (ts *TestDefaultAccessPolicySuite) TestMarshalYAML() {
	var tests = []*struct {
		name string

		value DefaultAccessPolicyT

		expectedYAML  string
		expectedError error
	}{
		{
			name:          "test Success Undefined",
			value:         DefaultAccessPolicyUndefined,
			expectedError: nil,
			expectedYAML:  `""\n`,
		},
		{
			name:          "test Success Allow",
			value:         DefaultAccessPolicyAllow,
			expectedError: nil,
			expectedYAML:  `"allow"\n`,
		},
		{
			name:          "test Success Deny",
			value:         DefaultAccessPolicyDeny,
			expectedError: nil,
			expectedYAML:  `"deny"\nf`,
		},
	}

	for _, test := range tests {
		serialized, gotErr := yaml.Marshal(test.value)

		if !ts.Equalf(test.expectedError, gotErr, "test %q", test.name) {
			ts.FailNowf("Aborted", "test %q", test.name)
		}

		ts.YAMLEqf(test.expectedYAML, string(serialized), "test %q", test.name)
	}
}

func (ts *TestDefaultAccessPolicySuite) TestUnmarshalYAML() {
	var tests = []*struct {
		name string

		YAML string

		expectedValue DefaultAccessPolicyT
		expectedError error
	}{
		{
			name:          "test Success Undefined",
			YAML:          `""`,
			expectedError: nil,
			expectedValue: DefaultAccessPolicyUndefined,
		},
		{
			name:          "test Success Allow",
			YAML:          `"allow"`,
			expectedError: nil,
			expectedValue: DefaultAccessPolicyAllow,
		},
		{
			name:          "test Success Deny",
			YAML:          `"deny"`,
			expectedError: nil,
			expectedValue: DefaultAccessPolicyDeny,
		},
	}

	for _, test := range tests {
		var deserialized DefaultAccessPolicyT

		gotErr := yaml.Unmarshal([]byte(test.YAML), &deserialized)

		if !ts.Equalf(test.expectedError, gotErr, "test %q", test.name) {
			ts.FailNowf("Aborted", "test %q", test.name)
		}

		ts.Equalf(test.expectedValue, deserialized, "test %q", test.name)
	}
}

func TestDefaultAccessPolicy(t *testing.T) {
	suite.Run(t, &TestDefaultAccessPolicySuite{})
}
