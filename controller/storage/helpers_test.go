package storage

import (
	"testing"

	"github.com/bxcodec/faker/v3"
)

func BenchmarkResourceNamesToSet8(b *testing.B) {
	// Prepare data.
	var source []ResourceNameT

	if err := faker.SetRandomMapAndSliceSize(8); err != nil {
		b.Fatalf("Can not faker.SetRandomMapAndSliceSize: %s", err)
	}

	if err := faker.FakeData(&source); err != nil {
		b.Fatalf("Can not faker.FakeData: %s", err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_ = ResourceNamesToSet(source)
	}
}

func BenchmarkResourceNamesToSet16(b *testing.B) {
	// Prepare data.
	var source []ResourceNameT

	if err := faker.SetRandomMapAndSliceSize(8); err != nil {
		b.Fatalf("Can not faker.SetRandomMapAndSliceSize: %s", err)
	}

	if err := faker.FakeData(&source); err != nil {
		b.Fatalf("Can not faker.FakeData: %s", err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_ = ResourceNamesToSet(source)
	}
}

func BenchmarkResourceNamesToSet32(b *testing.B) {
	// Prepare data.
	var source []ResourceNameT

	if err := faker.SetRandomMapAndSliceSize(8); err != nil {
		b.Fatalf("Can not faker.SetRandomMapAndSliceSize: %s", err)
	}

	if err := faker.FakeData(&source); err != nil {
		b.Fatalf("Can not faker.FakeData: %s", err)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_ = ResourceNamesToSet(source)
	}
}
