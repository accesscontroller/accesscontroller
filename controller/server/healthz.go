package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func createHealthzHandler(hc HealthChecker) echo.HandlerFunc {
	return func(c echo.Context) error {
		if err := hc.CheckHealthContext(c.Request().Context()); err != nil {
			return echo.ErrInternalServerError.SetInternal(err)
		}

		return c.JSON(http.StatusOK, echo.Map{
			"health": "ok",
		})
	}
}
