/*
Package storage contains resource types which are common for AccessController.
The package also contains interfaces which are used in AccessController's handlers.
*/
package storage

import (
	"encoding/json"
	"fmt"
	"reflect"
)

// WebResourceDomainT - Domain of Web Resource.
type WebResourceDomainT string

// ResourceNameT - general name of Resource
type ResourceNameT string

// APIVersionT - contains Resource's version
type APIVersionT string

// ResourceKindT - contains resource's Kind i.e. Resource type (for ex AccessGroup, ExternalGroup etc.)
type ResourceKindT string

// ETagT - Incrementing resource's version. Used to recognize resource's update
type ETagT int64

// Increment - Increments resource's version
func (et *ETagT) Increment() ETagT {
	*et++

	return *et
}

// WebResourceCategoryDescriptionT - contains textual description of WebResourceCategory
type WebResourceCategoryDescriptionT string

// WebResourceDescriptionT - contains textual description of WebResource
type WebResourceDescriptionT string

// ExtraAccessDenyMessageT - the message could be shown to a user on an "Access Denied" page
type ExtraAccessDenyMessageT string

// ExternalSessionsSourceGetModeT - Defines way (active/passive)
// by which Access Controller will get resource
type ExternalSessionsSourceGetModeT string

// ExternalSessionsSourceGetModeT - Defines way (active/passive)
// by which Access Controller will get resource
type ExternalUsersGroupsSourceGetModeT string

// ExternalSessionsSourceTypeT - Defines ExternalSessionsSourceType
// The type is used by Access Controller to get sessions information
type ExternalSessionsSourceTypeT string

// ExternalSessionsSourceTypeT - Defines ExternalSessionsSourceType
// The type is used by Access Controller to get sessions information
type ExternalUsersSourceTypeT string

// ExternalSessionsSourceTypeT - Defines ExternalSessionsSourceType
// The type is used by Access Controller to get sessions information
type ExternalUsersGroupsSourceTypeT string

const (
	ExternalUsersGroupsGetModePoll    ExternalUsersGroupsSourceGetModeT = "poll"         // AC makes requests to get data
	ExternalUsersGroupsGetModePassive ExternalUsersGroupsSourceGetModeT = "passive wait" // AC waits for pushes

	ExternalSessionsGetModePoll    ExternalSessionsSourceGetModeT = "poll"         // AC makes requests to get data
	ExternalSessionsGetModePassive ExternalSessionsSourceGetModeT = "passive wait" // AC waits for pushes

	ExternalSessionsSourceTypeMSADEvents ExternalSessionsSourceTypeT    = "MSADEvents" // Logstash as a source
	ExternalUsersGroupsSourceTypeLDAP    ExternalUsersGroupsSourceTypeT = "LDAP"       // LDAP source

	APIVersionV1Value APIVersionT = "v1" // API Version. V1 only exists now
)

// ExternalSourceInfo - Represents info about external resource source (i.e. ExternalGroupsSourceV1)
type ExternalSourceInfo struct {
	SourceName ResourceNameT
	Kind       ResourceKindT
}

// NewExternalSourceInfo - creates a new ExternalSourceInfo
func NewExternalSourceInfo(sourceName ResourceNameT, kind ResourceKindT) *ExternalSourceInfo {
	return &ExternalSourceInfo{
		SourceName: sourceName,
		Kind:       kind,
	}
}

func (esi *ExternalSourceInfo) String() string {
	if esi == nil {
		return "nil"
	}

	return fmt.Sprintf("{ SourceName: %s, Kind: %s }",
		esi.SourceName, esi.Kind)
}

// MetadataV1 - Standard Metadata fields to uniquely identify resources
type MetadataV1 struct {
	APIVersion       APIVersionT         `faker:"api_version" json:"apiVersion,omitempty" yaml:"apiVersion,omitempty"`
	Kind             ResourceKindT       `faker:"-" json:"kind,omitempty" yaml:"kind,omitempty"`
	Name             ResourceNameT       `faker:"unique" json:"name,omitempty" yaml:"name,omitempty"`
	ETag             ETagT               `faker:"e_tag" json:"eTag,omitempty" yaml:"eTag,omitempty"`
	ExternalResource bool                `faker:"-" json:"externalResource,omitempty" yaml:"externalResource,omitempty"`
	ExternalSource   *ExternalSourceInfo `faker:"-" json:"externalSource,omitempty" yaml:"externalSource,omitempty"`
}

// NewMetadataV1 - Creates a new MetadataV1.
// If externalSourceInfo != nil - assume external resource: set ExternalResource = true
func NewMetadataV1(kind ResourceKindT, name ResourceNameT,
	externalSourceInfo *ExternalSourceInfo) MetadataV1 {
	externalResource := externalSourceInfo != nil
	if externalResource {
		tESI := *externalSourceInfo

		return MetadataV1{
			APIVersion:       APIVersionV1Value,
			Kind:             kind,
			Name:             name,
			ETag:             0,
			ExternalResource: externalResource,
			ExternalSource:   &tESI,
		}
	}

	return MetadataV1{
		APIVersion:       APIVersionV1Value,
		Kind:             kind,
		Name:             name,
		ETag:             0,
		ExternalResource: externalResource,
		ExternalSource:   nil,
	}
}

// Copy - makes full copy of MetadataV1
func (mdv1 MetadataV1) Copy() MetadataV1 {
	var es *ExternalSourceInfo

	// If External Source is set
	if mdv1.ExternalSource != nil {
		es = &ExternalSourceInfo{
			SourceName: mdv1.ExternalSource.SourceName,
			Kind:       mdv1.ExternalSource.Kind,
		}
	}

	return MetadataV1{
		APIVersion:       mdv1.APIVersion,
		Kind:             mdv1.Kind,
		Name:             mdv1.Name,
		ETag:             mdv1.ETag,
		ExternalResource: mdv1.ExternalResource,
		ExternalSource:   es,
	}
}

func (mdv1 MetadataV1) ToJSON() (string, error) {
	bts, err := json.Marshal(mdv1)
	return string(bts), err
}

func (mdv1 MetadataV1) String() string {
	return fmt.Sprintf("{ Name: %s, ApiVersion: %v, Kind: %s, ETag: %d, ExternalResource: %t, ExternalSource: %v }",
		mdv1.Name, mdv1.APIVersion, mdv1.Kind, mdv1.ETag, mdv1.ExternalResource, mdv1.ExternalSource)
}

func (mdv1 MetadataV1) Equal(md MetadataV1) bool {
	return reflect.DeepEqual(mdv1, md)
}

// MetadataV1Getter - interface of object which could return its metadata
type MetadataV1Getter interface {
	GetMetadata() MetadataV1
}
