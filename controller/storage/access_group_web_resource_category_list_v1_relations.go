package storage

const (
	AccessGroup2WebResourceCategoryListV1RelationsKind ResourceKindT = "AccessGroup2WebResourceCategoryListRelations"
)

type AccessGroup2WebResourceCategoryListRelationsDataV1 struct {
	AccessGroup ResourceNameT   `json:"accessGroup,omitempty" yaml:"accessGroup,omitempty"`
	Categories  []ResourceNameT `json:"categories,omitempty" yaml:"categories,omitempty"`
}

type AccessGroup2WebResourceCategoryListRelationsV1 struct {
	Metadata MetadataV1                                         `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Data     AccessGroup2WebResourceCategoryListRelationsDataV1 `json:"data,omitempty" yaml:"data,omitempty"`
}

func NewAccessGroup2WebResourceCategoryListRelationsV1(name ResourceNameT) *AccessGroup2WebResourceCategoryListRelationsV1 {
	return &AccessGroup2WebResourceCategoryListRelationsV1{
		Metadata: MetadataV1{
			APIVersion: APIVersionV1Value,
			Kind:       AccessGroup2WebResourceCategoryListV1RelationsKind,
			Name:       name,
		},
		Data: AccessGroup2WebResourceCategoryListRelationsDataV1{
			Categories: make([]ResourceNameT, 0),
		},
	}
}
