package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func createGetExternalGroupV1(stor ExternalGroupV1Getter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Group name.
		name, err := parseExternalGroupName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Source name.
		source, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get Data.
		list, err := stor.GetExternalGroupV1Context(c.Request().Context(),
			name, source)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, list)
	}
}
