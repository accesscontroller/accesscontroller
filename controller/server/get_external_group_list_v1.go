package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createGetExternalGroupListV1(stor ExternalGroupV1sGetter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get source name.
		sourceName, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get Filter.
		var flt []storage.ExternalGroupFilterV1

		if err := c.Bind(&flt); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get Data.
		list, err := stor.GetExternalGroupV1sContext(c.Request().Context(),
			flt, sourceName)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, list)
	}
}
