package storage

import (
	"encoding/json"
	"fmt"

	"gopkg.in/yaml.v3"
)

/* DefaultAccessPolicyT - Access policy which is applied to all WebResources
which are not in AccessGroupV1#WebResourceCategories.
*/
type DefaultAccessPolicyT int

// IsAccessAllowed returns true if DefatultAccessPolicy value allows access.
func (dap DefaultAccessPolicyT) IsAccessAllowed() bool {
	return dap == DefaultAccessPolicyAllow
}

func (dap DefaultAccessPolicyT) MarshalJSON() ([]byte, error) {
	var tmpVal string

	switch dap {
	case DefaultAccessPolicyAllow:
		tmpVal = "allow"
	case DefaultAccessPolicyDeny:
		tmpVal = "deny"
	case DefaultAccessPolicyUndefined:
		tmpVal = ""
	default:
		return nil, &json.UnsupportedValueError{
			Str: fmt.Sprintf("can not MarshalJSON DefatultAccessPolicyT %d", dap),
		}
	}

	return json.Marshal(tmpVal)
}

func (dap *DefaultAccessPolicyT) UnmarshalJSON(b []byte) error {
	var tmpVal string

	if err := json.Unmarshal(b, &tmpVal); err != nil {
		return fmt.Errorf("can not UnmarshalJSON DefatultAccessPolicyT %q", string(b))
	}

	switch tmpVal {
	case "deny":
		*dap = DefaultAccessPolicyDeny
	case "allow":
		*dap = DefaultAccessPolicyAllow
	case "":
		*dap = DefaultAccessPolicyUndefined
	default:
		return fmt.Errorf("can not UnmarshalJSON DefatultAccessPolicyT %q", string(b))
	}

	return nil
}

func (dap DefaultAccessPolicyT) MarshalYAML() (interface{}, error) {
	switch dap {
	case DefaultAccessPolicyAllow:
		return "allow", nil
	case DefaultAccessPolicyDeny:
		return "deny", nil
	case DefaultAccessPolicyUndefined:
		return "", nil
	default:
		return nil, &yaml.TypeError{
			Errors: []string{
				fmt.Sprintf("can not MarshalYAML DefatultAccessPolicyT %d", dap),
			},
		}
	}
}

func (dap *DefaultAccessPolicyT) UnmarshalYAML(value *yaml.Node) error {
	var tmpVal string

	if err := value.Decode(&tmpVal); err != nil {
		return fmt.Errorf("can not UnmarshalYAML DefatultAccessPolicyT %q", value.Value)
	}

	switch tmpVal {
	case "deny":
		*dap = DefaultAccessPolicyDeny
	case "allow":
		*dap = DefaultAccessPolicyAllow
	case "":
		*dap = DefaultAccessPolicyUndefined
	default:
		return &yaml.TypeError{
			Errors: []string{
				fmt.Sprintf("can not UnmarshalYAML DefatultAccessPolicyT %q", value.Value),
			},
		}
	}

	return nil
}
