package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPatchExternalUserV1(stor ExternalUserV1Updater) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse params.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Parse User Name.
		name, err := parseExternalUserName(c)
		if err != nil {
			return err
		}

		// Parse external source name.
		sourceName, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return err
		}

		// Parse patch.
		var user = &storage.ExternalUserV1{}
		if err := c.Bind(user); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Validate user name and external source name.
		if sourceName != user.Metadata.ExternalSource.SourceName {
			return NewErrDifferentSources(sourceName,
				user.Metadata.ExternalSource.SourceName, user).HTTPErr()
		}

		// Update.
		user, err = stor.UpdateExternalUserV1Context(
			c.Request().Context(), name, user, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, user)
	}
}
