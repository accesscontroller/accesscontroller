package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createDeleteExternalSessionsSourceV1(stor ExternalSessionsSourceV1Deleter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// External Sessions Source Name.
		name, err := parseExternalSessionsSourceName(c)
		if err != nil {
			return err
		}

		// Get data.
		var ess = &storage.ExternalSessionsSourceV1{}
		if err := c.Bind(ess); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check Name.
		if name != ess.Metadata.Name {
			return NewErrNamesDoNotMatch(name, ess.Metadata.Name, ess).HTTPErr()
		}

		// Delete.
		err = stor.DeleteExternalSessionsSourceV1Context(
			c.Request().Context(), ess)
		if err != nil {
			return parseError(err)
		}

		return c.NoContent(http.StatusNoContent)
	}
}
