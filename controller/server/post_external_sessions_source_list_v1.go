package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostExternalSessionsSourceListV1(stor ExternalSessionsSourceV1sCreator) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse query parameters.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Read payload.
		var sources []storage.ExternalSessionsSourceV1

		if err := c.Bind(&sources); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Create.
		created, err := stor.CreateExternalSessionsSourceV1sContext(
			c.Request().Context(), sources, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, created)
	}
}
