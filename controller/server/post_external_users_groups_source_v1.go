package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostExternalUsersGroupsSourceV1(stor ExternalUsersGroupsSourceV1Creator) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse query parameters.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Get name.
		sourceName, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Parse request.
		var source = &storage.ExternalUsersGroupsSourceV1{}
		if err := c.Bind(source); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check name match.
		if sourceName != source.Metadata.Name {
			iErr := echo.HTTPError{
				Code:    http.StatusBadRequest,
				Message: "Name from URL must match with name from body#Metadata",
			}

			return &iErr
		}

		// Create.
		created, err := stor.CreateExternalUsersGroupsSourceV1Context(
			c.Request().Context(), source, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, created)
	}
}
