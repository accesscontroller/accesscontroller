package storage

import (
	"fmt"
)

const (
	AccessGroupKind       ResourceKindT = "AccessGroup"
	AccessGroupFilterKind ResourceKindT = "AccessGroupFilter"
)

const (
	DefaultAccessPolicyAllow     DefaultAccessPolicyT = 1
	DefaultAccessPolicyDeny      DefaultAccessPolicyT = 2
	DefaultAccessPolicyUndefined DefaultAccessPolicyT = 0
)

// AccessGroupV1 - contains AccessGroupDataV1
type AccessGroupV1 struct {
	Metadata MetadataV1        `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Data     AccessGroupDataV1 `json:"data,omitempty" yaml:"data,omitempty"`
}

func (g *AccessGroupV1) String() string {
	return fmt.Sprintf("%+v", *g)
}

// NewAccessGroupV1 - creates a new AccessGroupV1
func NewAccessGroupV1(name ResourceNameT) *AccessGroupV1 {
	return &AccessGroupV1{
		Metadata: NewMetadataV1(AccessGroupKind, name, nil),
		Data:     newAccessGroupDataV1(),
	}
}

// AccessGroupDataV1 - contains one access control rule which has applying order
type AccessGroupDataV1 struct {
	// DefaultPolicy defines action for resources not listed in DefaultPolicyExceptions.
	DefaultPolicy DefaultAccessPolicyT `faker:"default_access_policy" json:"defaultPolicy,omitempty" yaml:"defaultPolicy,omitempty"`
	// OrderInAccessRulesList defines order of applying this policy in policy list.
	OrderInAccessRulesList int64 `faker:"unique,boundary_start=1,boundary_end=100000000" json:"orderInAccessRulesList,omitempty" yaml:"orderInAccessRulesList,omitempty"`
	// TotalGroupSpeedLimitBps defines speed limit for all people who are under the group (summary).
	TotalGroupSpeedLimitBps int64 `faker:"boundary_start=1,boundary_end=100000000" json:"totalGroupSpeedLimitBps,omitempty" yaml:"totalGroupSpeedLimitBps,omitempty"`
	// UserGroupSpeedLimitBps defines limit for every user in the group.
	UserGroupSpeedLimitBps int64 `faker:"boundary_start=1,boundary_end=1000000" json:"userGroupSpeedLimitBps,omitempty" yaml:"userGroupSpeedLimitBps,omitempty"`
	// ExtraAccessDenyMessage defines a message to show a user in case of blocking access.
	ExtraAccessDenyMessage ExtraAccessDenyMessageT `faker:"len=50" json:"extraAccessDenyMessage,omitempty" yaml:"extraAccessDenyMessage,omitempty"`

	// ExternalGroups          []MetadataV1  // groups to match user from them
	// DefaultPolicyExceptions ResourceNameT // web resource categories to exclude them from DefaultPolicy
}

func (d *AccessGroupDataV1) String() string {
	return fmt.Sprintf("%+v", *d)
}

func newAccessGroupDataV1() AccessGroupDataV1 {
	return AccessGroupDataV1{
		DefaultPolicy: DefaultAccessPolicyDeny,
	}
}

// AccessGroupFilterDataV1 - contains fields which could be used to perform filtering
type AccessGroupFilterV1 struct {
	Name                   ResourceNameT        `json:"name,omitempty" yaml:"name,omitempty"`
	OrderInAccessRulesList *int64               `json:"orderInAccessRulesList,omitempty" yaml:"orderInAccessRulesList,omitempty"`
	DefaultPolicy          DefaultAccessPolicyT `json:"defaultPolicy,omitempty" yaml:"defaultPolicy,omitempty"`
}

func (d *AccessGroupFilterV1) String() string {
	return fmt.Sprintf("%+v", *d)
}
