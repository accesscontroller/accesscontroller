package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostExternalSessionsSourceV1(stor ExternalSessionsSourceV1Creator) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse query parameters.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Get name.
		name, err := parseExternalSessionsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Read payload.
		var source = &storage.ExternalSessionsSourceV1{}
		if err := c.Bind(source); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check name match.
		if name != source.Metadata.Name {
			return NewErrNamesDoNotMatch(
				name, source.Metadata.Name, source).HTTPErr()
		}

		// Create.
		created, err := stor.CreateExternalSessionsSourceV1Context(
			c.Request().Context(), source, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, created)
	}
}
