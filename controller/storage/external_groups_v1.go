package storage

import (
	"fmt"
)

const (
	ExternalGroupKind       ResourceKindT = "ExternalGroup"
	ExternalGroupFilterKind ResourceKindT = "ExternalGroupFilter"
)

// ExternalGroupV1 - One External (imported from ExternalGroupsSource) group
type ExternalGroupV1 struct {
	Metadata MetadataV1          `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Data     ExternalGroupDataV1 `json:"data,omitempty" yaml:"data,omitempty"`
}

func (egv1 *ExternalGroupV1) String() string {
	return fmt.Sprintf("%+v", *egv1)
}

// NewExternalGroupV1 - creates a new ExternalGroupV1
func NewExternalGroupV1(name ResourceNameT, groupsSource ResourceNameT) *ExternalGroupV1 {
	return &ExternalGroupV1{
		Metadata: NewMetadataV1(ExternalGroupKind,
			name, NewExternalSourceInfo(groupsSource, ExternalUsersGroupsSourceKind)),
	}
}

// GetMetadata - returns copy of ExternalGroupV1 MetadataV1
func (egv1 *ExternalGroupV1) GetMetadata() MetadataV1 {
	return egv1.Metadata.Copy()
}

type ExternalGroupDataV1 struct {
	AccessGroupName ResourceNameT `faker:"-" json:"accessGroupName,omitempty" yaml:"accessGroupName,omitempty"`

	// ExternalUsers []ResourceNameT
}

func (v *ExternalGroupDataV1) String() string {
	return fmt.Sprintf("%+v", *v)
}

// ExternalGroupFilterV1 - filter to get filtered ExternalGroups.
type ExternalGroupFilterV1 struct {
	Name ResourceNameT `json:"name,omitempty" yaml:"name,omitempty"`
}

func (eglfv1 *ExternalGroupFilterV1) String() string {
	return fmt.Sprintf("%+v", *eglfv1)
}
