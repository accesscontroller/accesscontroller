package server

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func parseExternalUserName(c echo.Context) (storage.ResourceNameT, error) {
	eun := storage.ResourceNameT(c.Param(externalUserNamePathParam))
	if len(eun) == 0 {
		return "", ErrEmptyExternalUserNamePathParam
	}

	return eun, nil
}

func parseExternalGroupName(c echo.Context) (storage.ResourceNameT, error) {
	eun := storage.ResourceNameT(c.Param(externalGroupNamePathParam))
	if len(eun) == 0 {
		return "", ErrEmptyExternalGroupNamePathParam
	}

	return eun, nil
}

func parseExternalUsersGroupsSourceName(c echo.Context) (storage.ResourceNameT, error) {
	eusn := storage.ResourceNameT(c.Param(externalUsersGroupsSourceNamePathParam))
	if len(eusn) == 0 {
		return "", ErrEmptyExternalUsersGroupsSourceNamePathParam
	}

	return eusn, nil
}

func parseWebResourceCategoryName(c echo.Context) storage.ResourceNameT {
	return storage.ResourceNameT(c.Param(webResourceCategoryNamePathParam))
}

func parseAccessGroupName(c echo.Context) (storage.ResourceNameT, error) {
	eun := storage.ResourceNameT(c.Param(accessGroupNamePathParam))
	if len(eun) == 0 {
		return "", ErrEmptyAccessGroupNamePathParam
	}

	return eun, nil
}

func parseExternalUserSessionName(c echo.Context) (storage.ResourceNameT, error) {
	eusn := storage.ResourceNameT(c.Param(externalUserSessionNamePathParam))
	if len(eusn) == 0 {
		return "", ErrEmptyExternalUserSessionNamePathParam
	}

	return eusn, nil
}

func parseWebResourceName(c echo.Context) (storage.ResourceNameT, error) {
	wrn := storage.ResourceNameT(c.Param(webResourceNamePathParam))
	if len(wrn) == 0 {
		return "", ErrEmptyWebResourceNamePathParam
	}

	return wrn, nil
}

func parseExternalSessionsSourceName(c echo.Context) (storage.ResourceNameT, error) {
	wrn := storage.ResourceNameT(c.Param(externalSessionsSourceNamePathParam))
	if len(wrn) == 0 {
		return "", ErrEmptyExternalSessionsSourceNamePathParam
	}

	return wrn, nil
}
