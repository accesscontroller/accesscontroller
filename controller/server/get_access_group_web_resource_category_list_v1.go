package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createGetAccessGroupWebResourceCategoryListV1(stor AccessGroup2WebResourceCategoryV1RelationGetter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Access Group name.
		accessGroup, err := parseAccessGroupName(c)
		if err != nil {
			return err
		}

		webResCats, err := stor.GetAccessGroupWebResourceCategoryV1sContext(
			c.Request().Context(), accessGroup)
		if err != nil {
			return err
		}

		// Result.
		var res = storage.NewAccessGroup2WebResourceCategoryListRelationsV1(accessGroup)

		res.Data.Categories = webResCats
		res.Data.AccessGroup = accessGroup

		return c.JSON(http.StatusOK, &res)
	}
}
