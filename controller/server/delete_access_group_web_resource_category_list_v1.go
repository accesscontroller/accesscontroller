package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createDeleteAccessGroupWebResourceCategoryListV1(stor AccessGroup2WebResourceCategoryV1RelationUnBinder) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse Access Group.
		accessGroup, err := parseAccessGroupName(c)
		if err != nil {
			return err
		}

		// Parse Relations payload.
		var relations storage.AccessGroup2WebResourceCategoryListRelationsV1

		if err := c.Bind(&relations); err != nil {
			return err
		}

		if accessGroup != relations.Data.AccessGroup {
			return &echo.HTTPError{
				Code:    http.StatusBadRequest,
				Message: "Path AccessGroup name is not equal to AccessGroup2WebResourceCategoryListRelationsV1#AccessGroup",
			}
		}

		if err := stor.UnBindAccessGroup2WebResourceCategoryV1sContext(
			c.Request().Context(), accessGroup, relations.Data.Categories); err != nil {
			return parseError(err)
		}

		return nil
	}
}
