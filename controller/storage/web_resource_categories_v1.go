package storage

import (
	"fmt"
)

const (
	WebResourceCategoryKind         ResourceKindT = "WebResourceCategory"
	WebResourceCategoryFilterV1Kind ResourceKindT = "WebResourceCategoryFilter"
)

// WebResourceCategoryDataV1 - contains data of one WebResourceCategoryV1.
type WebResourceCategoryDataV1 struct {
	Description WebResourceCategoryDescriptionT `json:"description,omitempty" yaml:"description,omitempty"`
}

func (v *WebResourceCategoryDataV1) String() string {
	return fmt.Sprintf("%+v", *v)
}

// WebResourceCategoryV1 - one WebResourceCategory.
type WebResourceCategoryV1 struct {
	Metadata MetadataV1                `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Data     WebResourceCategoryDataV1 `json:"data,omitempty" yaml:"data,omitempty"`
}

func (wrcv1 *WebResourceCategoryV1) String() string {
	return fmt.Sprintf("%+v", *wrcv1)
}

// NewWebResourceCategoryV1 - creates a new WebResourceCategoryV1.
func NewWebResourceCategoryV1(name ResourceNameT) *WebResourceCategoryV1 {
	return &WebResourceCategoryV1{
		Metadata: NewMetadataV1(WebResourceCategoryKind, name, nil),
	}
}

// GetMetadata - returns copy of WebResourceCategoryV1 MetadataV1.
func (wrcv1 *WebResourceCategoryV1) GetMetadata() MetadataV1 {
	return wrcv1.Metadata.Copy()
}

// WebResourceCategoryFilterV1 - contains one set of WebResourceCategoryFilterV1 fields.
// Filtering are made using logical AND.
type WebResourceCategoryFilterV1 struct {
	Name                      ResourceNameT `json:"name,omitempty" yaml:"name,omitempty"`
	NameSubstringMatch        ResourceNameT `json:"nameSubstringMatch,omitempty" yaml:"nameSubstringMatch,omitempty"`
	DescriptionSubstringMatch string        `json:"descriptionSubstringMatch,omitempty" yaml:"descriptionSubstringMatch,omitempty"`
}

func (v *WebResourceCategoryFilterV1) String() string {
	return fmt.Sprintf("%+v", *v)
}
