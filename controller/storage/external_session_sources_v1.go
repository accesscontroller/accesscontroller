package storage

import (
	"fmt"
)

const (
	ExternalSessionsSourceKind       ResourceKindT = "ExternalSessionsSource"
	ExternalSessionsSourceFilterKind ResourceKindT = "ExternalSessionsSourceFilter"
)

// ExternalSessionsSourceV1 - one source of External Sessions V1.
type ExternalSessionsSourceV1 struct {
	Metadata MetadataV1                   `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Data     ExternalSessionsSourceDataV1 `json:"data,omitempty" yaml:"data,omitempty"`
}

func (essv1 *ExternalSessionsSourceV1) String() string {
	return fmt.Sprintf("%+v", *essv1)
}

// NewExternalSessionsSourceV1 - creates a new ExternalSessionsSourceV1.
func NewExternalSessionsSourceV1(name ResourceNameT) *ExternalSessionsSourceV1 {
	return &ExternalSessionsSourceV1{
		Metadata: NewMetadataV1(ExternalSessionsSourceKind, name, nil),
	}
}

// ExternalSessionsSourceDataV1 - parameters to get ExternalSessionV1s.
type ExternalSessionsSourceDataV1 struct {
	GetMode                 ExternalSessionsSourceGetModeT `json:"getMode,omitempty" yaml:"getMode,omitempty"`
	Type                    ExternalSessionsSourceTypeT    `json:"type,omitempty" yaml:"type,omitempty"`
	ExternalUsersSourceName ResourceNameT                  `json:"externalUsersSourceName,omitempty" yaml:"externalUsersSourceName,omitempty"`
	Settings                map[string]string              `json:"settings,omitempty" yaml:"settings,omitempty"`
}

func (v *ExternalSessionsSourceDataV1) String() string {
	return fmt.Sprintf("%+v", *v)
}

// ExternalSessionsSourceFilterDataV1 - contains fields to filter ExternalSessionsSourceV1s.
// Set fields are processed using logical AND.
type ExternalSessionsSourceFilterV1 struct {
	Name    ResourceNameT                  `json:"name,omitempty" yaml:"name,omitempty"`
	GetMode ExternalSessionsSourceGetModeT `json:"getMode,omitempty" yaml:"getMode,omitempty"`
	Type    ExternalSessionsSourceTypeT    `json:"type,omitempty" yaml:"type,omitempty"`
}

func (v *ExternalSessionsSourceFilterV1) String() string {
	return fmt.Sprintf("%+v", *v)
}
