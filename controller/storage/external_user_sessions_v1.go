package storage

import (
	"encoding/json"
	"fmt"
	"net"
	"time"
)

const (
	ExternalUserSessionKind       ResourceKindT = "ExternalUserSession"
	ExternalUserSessionFilterKind ResourceKindT = "ExternalUserSessionFilter"
)

// ExternalUserSessionV1 - one session of ExternalUserV1.
type ExternalUserSessionV1 struct {
	Metadata MetadataV1                `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Data     ExternalUserSessionDataV1 `json:"data,omitempty" yaml:"data,omitempty"`
}

func (eusv1 *ExternalUserSessionV1) String() string {
	return fmt.Sprintf("%+v", *eusv1)
}

// NewExternalUserSessionV1 - creates a new ExternalUserSession.
func NewExternalUserSessionV1(name ResourceNameT,
	sourceName ResourceNameT) *ExternalUserSessionV1 {
	return &ExternalUserSessionV1{
		Metadata: NewMetadataV1(ExternalUserSessionKind, name,
			NewExternalSourceInfo(sourceName, ExternalSessionsSourceKind)),
	}
}

// ExternalUserSessionDataV1 - contains session's data.
type ExternalUserSessionDataV1 struct {
	Closed         bool       `json:"closed,omitempty" yaml:"closed,omitempty"`
	OpenTimestamp  *time.Time `json:"openTimestamp,omitempty" yaml:"openTimestamp,omitempty"`
	CloseTimestamp *time.Time `json:"closeTimestamp,omitempty" yaml:"closeTimestamp,omitempty"`
	Hostname       string     `json:"hostname,omitempty" yaml:"hostname,omitempty"`
	IPAddress      net.IP     `json:"ipAddress,omitempty" yaml:"ipAddress,omitempty"`

	ExternalUserName              ResourceNameT `json:"externalUserName,omitempty" yaml:"externalUserName,omitempty"`
	ExternalUsersGroupsSourceName ResourceNameT `json:"externalUsersGroupsSourceName,omitempty" yaml:"externalUsersGroupsSourceName,omitempty"`
}

func (v *ExternalUserSessionDataV1) String() string {
	return fmt.Sprintf("%+v", *v)
}

// intExternalUserSessionFilterV1 - copy of type ExternalUserSessionFilterDataV1.
// Used in JSON marshaling.
type intExternalUserSessionFilterV1 struct {
	Closed                     *bool         `json:"closed,omitempty" yaml:"closed,omitempty"`
	OpenTimestamp              *time.Time    `json:"openTimestamp,omitempty" yaml:"openTimestamp,omitempty"`
	CloseTimestamp             *time.Time    `json:"closeTimestamp,omitempty" yaml:"closeTimestamp,omitempty"`
	ExternalSessionsSourceName ResourceNameT `json:"externalSessionsSourceName,omitempty" yaml:"externalSessionsSourceName,omitempty"`
	ExternalUserName           ResourceNameT `json:"externalUserName,omitempty" yaml:"externalUserName,omitempty"`
	Hostname                   string        `json:"hostname,omitempty" yaml:"hostname,omitempty"`
	IPAddress                  *string       `json:"ipAddress,omitempty" yaml:"ipAddress,omitempty"`
	Name                       ResourceNameT `json:"name,omitempty" yaml:"name,omitempty"`
}

// ExternalUserSessionFilterV1 - contains fields to perform filtering of ExternalUserSessionV1.
type ExternalUserSessionFilterV1 struct {
	Closed                     *bool         `json:"closed,omitempty" yaml:"closed,omitempty"`
	OpenTimestamp              *time.Time    `json:"openTimestamp,omitempty" yaml:"openTimestamp,omitempty"`
	CloseTimestamp             *time.Time    `json:"closeTimestamp,omitempty" yaml:"closeTimestamp,omitempty"`
	ExternalSessionsSourceName ResourceNameT `json:"externalSessionsSourceName,omitempty" yaml:"externalSessionsSourceName,omitempty"`
	ExternalUserName           ResourceNameT `json:"externalUserName,omitempty" yaml:"externalUserName,omitempty"`
	Hostname                   string        `json:"hostname,omitempty" yaml:"hostname,omitempty"`
	IPAddress                  net.IP        `json:"ipAddress,omitempty" yaml:"ipAddress,omitempty"`
	Name                       ResourceNameT `json:"name,omitempty" yaml:"name,omitempty"`
}

func (sfd *ExternalUserSessionFilterV1) String() string {
	return fmt.Sprintf("%+v", *sfd)
}

// MarshalJSON - implements JSON Marshaler.
func (sfd *ExternalUserSessionFilterV1) MarshalJSON() ([]byte, error) {
	var IPAP *string

	if sfd.IPAddress != nil {
		t := sfd.IPAddress.String()
		IPAP = &t
	}

	// Make copy
	c := intExternalUserSessionFilterV1{
		Closed:                     sfd.Closed,
		OpenTimestamp:              sfd.OpenTimestamp,
		CloseTimestamp:             sfd.CloseTimestamp,
		ExternalSessionsSourceName: sfd.ExternalSessionsSourceName,
		ExternalUserName:           sfd.ExternalUserName,
		Hostname:                   sfd.Hostname,
		IPAddress:                  IPAP,
		Name:                       sfd.Name,
	}

	return json.Marshal(c)
}

// UnmarshalJSON - implements JSON Unmarshaler.
func (sfd *ExternalUserSessionFilterV1) UnmarshalJSON(b []byte) error {
	// Parse JSON
	c := intExternalUserSessionFilterV1{}
	if err := json.Unmarshal(b, &c); err != nil {
		return err
	}

	// Parse IP
	var IP net.IP

	if c.IPAddress != nil && len(*c.IPAddress) > 0 {
		tip := net.ParseIP(*c.IPAddress)
		if tip == nil {
			return fmt.Errorf("invalid IPAddress: %q", *c.IPAddress)
		}

		IP = tip
	}

	// Make result
	sfd.Closed = c.Closed
	sfd.OpenTimestamp = c.OpenTimestamp
	sfd.CloseTimestamp = c.CloseTimestamp
	sfd.ExternalSessionsSourceName = c.ExternalSessionsSourceName
	sfd.ExternalUserName = c.ExternalUserName
	sfd.Hostname = c.Hostname
	sfd.IPAddress = IP
	sfd.Name = c.Name

	return nil
}
