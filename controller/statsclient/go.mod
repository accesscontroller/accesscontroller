module gitlab.com/accesscontroller/accesscontroller/controller/statsclient

go 1.12

require (
	github.com/labstack/echo v3.3.10+incompatible // indirect
	gitlab.com/accesscontroller/accesscontroller/controller/server v0.0.0-20190705091125-77e6da6a9e2c // indirect
)
