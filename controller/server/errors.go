package server

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

var (
	ErrNilStorage  = errors.New("nil given as Storage")
	ErrLoadTLSCert = errors.New("can not load TLS certificate")
)

type ErrDifferentSources struct {
	expected storage.ResourceNameT
	given    storage.ResourceNameT
	resource interface{}
}

func NewErrDifferentSources(exp storage.ResourceNameT,
	given storage.ResourceNameT, resource interface{}) *ErrDifferentSources {
	return &ErrDifferentSources{
		expected: exp,
		given:    given,
		resource: resource,
	}
}

func (e ErrDifferentSources) Error() string {
	return fmt.Sprintf("incorrect external source in %+v. Expected %s, given %s",
		e.resource, e.expected, e.given)
}

func (e ErrDifferentSources) HTTPErr() *echo.HTTPError {
	return &echo.HTTPError{
		Code:     http.StatusBadRequest,
		Message:  e.Error(),
		Internal: e,
	}
}

type ErrNamesDoesNotMatch struct {
	pathName     storage.ResourceNameT
	resourceName storage.ResourceNameT
	resource     interface{}
}

func NewErrNamesDoNotMatch(pathName storage.ResourceNameT,
	resourceName storage.ResourceNameT, resource interface{}) *ErrNamesDoesNotMatch {
	return &ErrNamesDoesNotMatch{
		pathName:     pathName,
		resourceName: resourceName,
		resource:     resource,
	}
}

func (e ErrNamesDoesNotMatch) Error() string {
	return fmt.Sprintf(
		"Names from resource and from request path are not equal. Path Name: %s; Resource Name: %s; Resource: %+v",
		e.pathName, e.resourceName, e.resource)
}

func (e ErrNamesDoesNotMatch) HTTPErr() *echo.HTTPError {
	return &echo.HTTPError{
		Code:     http.StatusBadRequest,
		Internal: e,
		Message:  e.Error(),
	}
}
