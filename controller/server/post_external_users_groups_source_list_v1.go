package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostExternalUsersGroupsSourceListV1(stor ExternalUsersGroupsSourceV1sCreator) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse query parameters.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Parse request.
		var sources []storage.ExternalUsersGroupsSourceV1

		if err := c.Bind(&sources); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Create.
		created, err := stor.CreateExternalUsersGroupsSourceV1sContext(
			c.Request().Context(), sources, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, created)
	}
}
