package storage

const (
	ExternalGroup2ExternalUserListV1RelationsKind ResourceKindT = "ExternalGroup2ExternalUserListRelations"
)

type ExternalGroup2ExternalUserListRelationsDataV1 struct {
	ExternalUsersGroupsSource ResourceNameT `json:"externalUsersGroupsSource,omitempty" yaml:"externalUsersGroupsSource,omitempty"`
	ExternalGroup             ResourceNameT `json:"externalGroup,omitempty" yaml:"externalGroup,omitempty"`

	ExternalUsers []ResourceNameT `json:"externalUsers,omitempty" yaml:"externalUsers,omitempty"`
}

type ExternalGroup2ExternalUserListRelationsV1 struct {
	Metadata MetadataV1                                    `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Data     ExternalGroup2ExternalUserListRelationsDataV1 `json:"data,omitempty" yaml:"data,omitempty"`
}

func NewExternalGroup2ExternalUserListRelationsV1(name ResourceNameT) *ExternalGroup2ExternalUserListRelationsV1 {
	return &ExternalGroup2ExternalUserListRelationsV1{
		Metadata: MetadataV1{
			APIVersion: APIVersionV1Value,
			Name:       name,
			Kind:       ExternalGroup2ExternalUserListV1RelationsKind,
		},
		Data: ExternalGroup2ExternalUserListRelationsDataV1{
			ExternalUsers: make([]ResourceNameT, 0),
		},
	}
}
