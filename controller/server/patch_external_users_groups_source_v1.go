package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPatchExternalUsersGroupsSourceV1(stor ExternalUsersGroupsSourceV1Updater) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Params.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Get name.
		name, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get patch.
		patch := &storage.ExternalUsersGroupsSourceV1{}
		if err := c.Bind(patch); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Perform update.
		updated, err := stor.UpdateExternalUsersGroupsSourceV1Context(
			c.Request().Context(), name, patch, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, updated)
	}
}
