package validator

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/getkin/kin-openapi/openapi3filter"
	"github.com/labstack/echo/v4"
)

// New - creates and returns validation middleware.
func New(spec []byte, validateRequest, validateResponse bool) (echo.MiddlewareFunc, error) {

	rtr, err := createRouter(spec)
	if err != nil {
		return nil, err
	}

	validatorOptions := &openapi3filter.Options{
		ExcludeRequestBody:  !validateRequest,
		ExcludeResponseBody: !validateResponse,
	}

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) (err error) {

			// Lookup route
			req := c.Request()

			rt, pathParams, err := rtr.FindRoute(req.Method, req.URL)
			if err != nil {
				return echo.ErrBadRequest.SetInternal(err)
			}

			reqValIn := &openapi3filter.RequestValidationInput{
				Request:     req,
				PathParams:  pathParams,
				QueryParams: req.URL.Query(),
				Route:       rt,
				Options:     validatorOptions,
			}

			// Request validation
			if validateRequest {
				// Request
				if err := openapi3filter.ValidateRequest(req.Context(), reqValIn); err != nil {
					switch tpd := err.(type) {
					case *openapi3filter.RequestError:
						te := &echo.HTTPError{
							Code:     http.StatusBadRequest,
							Internal: tpd,
							Message:  tpd.Error(),
						}
						return te
					default:
						return echo.ErrBadRequest.SetInternal(tpd)
					}
				}
			}

			// Validate response or error
			var processErr error
			if validateResponse {
				// Mock response writer
				respBody := newBufferedWriter(c.Response().Writer)
				c.Response().Writer = respBody

				// Restore original writer on normal and panic
				defer func() {
					c.Response().Writer = respBody.GetOriginalWriter()
				}()

				// Execute request handler
				processErr = next(c)
				if processErr != nil {
					c.Error(processErr)
				}

				// Prepare validator
				resp := &openapi3filter.ResponseValidationInput{
					RequestValidationInput: reqValIn,
					Status:                 c.Response().Status,
					Header:                 c.Response().Header(),
					Body:                   ioutil.NopCloser(respBody.GetCopyReader()),
					Options:                validatorOptions,
				}

				// Validate response
				validateError := openapi3filter.ValidateResponse(req.Context(), resp)
				if err := sendBufferedResponse(c, respBody, validateError); err != nil {
					return err
				}
			} else {
				// Execute request handler
				processErr = next(c)
				if processErr != nil {
					c.Error(processErr)
				}
			}

			return nil
		}
	}, nil
}

func createRouter(spec []byte) (*openapi3filter.Router, error) {
	loader := openapi3.NewSwaggerLoader()
	swag, err := loader.LoadSwaggerFromData(spec)
	if err != nil {
		return nil, err
	}

	rtr := openapi3filter.NewRouter()
	err = rtr.AddSwagger(swag)
	if err != nil {
		switch tpd := err.(type) {
		case *openapi3filter.ParseError:
			return nil, fmt.Errorf("OpenAPI spec parsing error %s at %s", tpd.RootCause(), tpd.Path())

		default:
			return nil, tpd
		}
	}

	return rtr, nil
}

func sendBufferedResponse(c echo.Context, respBody *buffedWriter, validateErr error) error {
	resp := c.Response()

	// Handle validation error
	if validateErr != nil {
		// Reset response buffer
		respBody.Reset()

		resp.Committed = false
		resp.WriteHeader(http.StatusInternalServerError)
		if _, err := resp.Write([]byte(fmt.Sprintf(`{"message": %q}`, validateErr.Error()))); err != nil {
			return fmt.Errorf("Error writing error: %s", err)
		}
	}

	// Flush
	if ierr := respBody.FlushToOriginal(); ierr != nil {
		return ierr
	}

	return nil
}
