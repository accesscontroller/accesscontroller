package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createDeleteExternalUsersGroupsSourceV1(stor ExternalUsersGroupsSourceV1Deleter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get Source name.
		name, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get source to delete.
		var source = &storage.ExternalUsersGroupsSourceV1{}
		if err := c.Bind(source); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check names match.
		if name != source.Metadata.Name {
			return NewErrNamesDoNotMatch(name, source.Metadata.Name, source).HTTPErr()
		}

		// Delete.
		if err := stor.DeleteExternalUsersGroupsSourceV1Context(
			c.Request().Context(), source); err != nil {
			return parseError(err)
		}

		return c.NoContent(http.StatusNoContent)
	}
}
