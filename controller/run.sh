#!/bin/sh
export "ACCESS_CONTROLLER_BCRYPT_DIFFICULTY"="4"
export "ACCESS_CONTROLLER_BIND"=":8080"

export "ACCESS_CONTROLLER_DB_LOGIN"="postgres"
export "ACCESS_CONTROLLER_DB_PASSWORD"="postgres"
export "ACCESS_CONTROLLER_DB_HOSTPORT"="localhost:5432"
export ACCESS_CONTROLLER_DB_NAME=accesscontroller
            



# Run
{
    cd `dirname $0`
    ./api_server -insecure-api=true
}