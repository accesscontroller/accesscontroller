package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostWebResourceListV1(stor WebResourceV1sCreator) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse query parameters.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Get payload.
		var resources []storage.WebResourceV1

		if err := c.Bind(&resources); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Create.
		newResources, err := stor.CreateWebResourceV1sContext(
			c.Request().Context(), resources, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, newResources)
	}
}
