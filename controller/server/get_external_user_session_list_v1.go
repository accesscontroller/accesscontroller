package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createGetExternalUserSessionListV1(stor ExternalUserSessionV1sGetter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get Sessions Source Name.
		ssName, err := parseExternalSessionsSourceName(c)
		if err != nil {
			return err
		}

		// Get filter.
		var flt []storage.ExternalUserSessionFilterV1

		if err := c.Bind(&flt); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get data
		sessions, err := stor.GetExternalUserSessionV1sContext(
			c.Request().Context(), ssName, flt)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, sessions)
	}
}
