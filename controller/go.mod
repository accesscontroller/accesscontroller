module gitlab.com/accesscontroller/accesscontroller/controller

go 1.12

require (
	github.com/casbin/casbin/v2 v2.19.7 // indirect
	github.com/getkin/kin-openapi v0.34.0 // indirect
	github.com/vmihailenco/msgpack/v5 v5.1.3 // indirect
	gitlab.com/accesscontroller/accesscontroller/controller/middleware/openapi_validator v0.0.0-20201227151802-89abbe05265a // indirect
	gitlab.com/accesscontroller/accesscontroller/controller/server v0.0.0-20201227151802-89abbe05265a
	gitlab.com/accesscontroller/sqlstorage3 v0.0.0-20201227151925-1354f23e0a01
	go.opentelemetry.io/otel v0.15.0 // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b // indirect
	golang.org/x/sys v0.0.0-20201223074533-0d417f636930 // indirect
)

replace gitlab.com/accesscontroller/accesscontroller/controller/server => ./server

replace gitlab.com/accesscontroller/sqlstorage3 => ../sqlstorage3
