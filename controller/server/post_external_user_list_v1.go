package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostExternalUserListV1(stor ExternalUserV1sCreator) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse query parameters.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Get source name.
		sName, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Parse.
		var usrList []storage.ExternalUserV1

		if err := c.Bind(&usrList); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check that source name is correct in Metadata.
		for i := range usrList {
			if sName != usrList[i].Metadata.ExternalSource.SourceName {
				return NewErrDifferentSources(sName,
					usrList[i].Metadata.ExternalSource.SourceName,
					usrList[i]).HTTPErr()
			}
		}

		// Create.
		users, err := stor.CreateExternalUserV1sContext(
			c.Request().Context(), usrList, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, users)
	}
}
