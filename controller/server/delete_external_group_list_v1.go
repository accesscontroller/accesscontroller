package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createDeleteExternalGroupListV1(stor ExternalGroupV1sDeleter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get Source name.
		sourceName, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Parse Group List.
		var extGroupList []storage.ExternalGroupV1

		if err := c.Bind(&extGroupList); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check Source.
		for i := range extGroupList {
			if sourceName != extGroupList[i].Metadata.ExternalSource.SourceName {
				return NewErrDifferentSources(sourceName,
					extGroupList[i].Metadata.ExternalSource.SourceName, extGroupList[i])
			}
		}

		// Delete and get result.
		if err := stor.DeleteExternalGroupV1sContext(c.Request().Context(), extGroupList); err != nil {
			return parseError(err)
		}

		return c.NoContent(http.StatusNoContent)
	}
}
