package server

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type TestServerConfigurationSuite struct {
	suite.Suite
}

func (ts *TestServerConfigurationSuite) TestNewDefatultConfigurationSuccess() {
	gotConfig, gotErr := NewDefaultConfiguration()

	asrt := ts.Assert()

	asrt.NoError(gotErr, "Must not return error")
	asrt.NotZero(gotConfig, "Must not return zero Configuration")
}

func TestServerConfiguration(t *testing.T) {
	suite.Run(t, &TestServerConfigurationSuite{})
}
