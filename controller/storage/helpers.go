package storage

import (
	"net"
)

func compareIPs(a, b net.IP) bool {
	if len(a) != len(b) {
		return false
	}

	for i, av := range a {
		if av != b[i] {
			return false
		}
	}

	return true
}

// ResourceNamesToSet converts a slice of ResourceNameT to a map-set with the slice's data.
func ResourceNamesToSet(v []ResourceNameT) map[ResourceNameT]bool {
	res := make(map[ResourceNameT]bool, len(v))

	for i := range v {
		res[v[i]] = true
	}

	return res
}

// CompareResourceNameSets compares two slices as sets of values.
func CompareResourceNameSets(set1, set2 []ResourceNameT) bool {
	m1 := ResourceNamesToSet(set1)
	m2 := ResourceNamesToSet(set2)

	for v1 := range m1 {
		if _, ok := m2[v1]; !ok {
			return false
		}
	}

	for v2 := range m2 {
		if _, ok := m1[v2]; !ok {
			return false
		}
	}

	return true
}
