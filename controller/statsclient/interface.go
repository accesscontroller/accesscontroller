package statsclient

import (
	"net"
	"time"
)

type AuthEventType string

const (
	EventLogon  AuthEventType = "logon"
	EventLogoff AuthEventType = "logoff"
)

type AuthEvent struct {
	Login     string
	Type      AuthEventType
	ClientIP  net.IP
	Timestamp time.Time

	SourceName string
}

type EventSender interface {
	SendEvent(ev *AuthEvent) error
}

type StatsGetter interface {
}
