package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPatchWebResourceV1(stor WebResourceV1Updater) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Params.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Name.
		name, err := parseWebResourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get payload
		var resource = &storage.WebResourceV1{}
		if err := c.Bind(resource); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Update
		updatedResource, err := stor.UpdateWebResourceV1Context(
			c.Request().Context(), name, resource, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, updatedResource)
	}
}
