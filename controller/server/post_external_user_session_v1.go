package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostExternalUserSessionV1(stor ExternalUserSessionV1Opener) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse query parameters.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Get session name.
		name, err := parseExternalUserSessionName(c)
		if err != nil {
			return err
		}

		// Get Sessions source name.
		sessSourceName, err := parseExternalSessionsSourceName(c)
		if err != nil {
			return err
		}

		// Get payload.
		var session = &storage.ExternalUserSessionV1{}
		if err := c.Bind(session); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check name.
		if name != session.Metadata.Name {
			return ErrNamesDoesNotMatch{
				pathName:     name,
				resourceName: session.Metadata.Name,
				resource:     session,
			}.HTTPErr()
		}

		// Check sessions source name.
		if sessSourceName != session.Metadata.ExternalSource.SourceName {
			return ErrDifferentSources{
				expected: sessSourceName,
				given:    session.Metadata.ExternalSource.SourceName,
				resource: session,
			}.HTTPErr()
		}

		// Create.
		newSession, err := stor.OpenExternalUserSessionV1Context(
			c.Request().Context(), session, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, newSession)
	}
}
