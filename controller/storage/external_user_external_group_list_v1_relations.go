package storage

const (
	ExternalUser2ExternalGroupListV1RelationsKind ResourceKindT = "ExternalUser2ExternalGroupListV1Relations"
)

type ExternalUser2ExternalGroupListRelationsDataV1 struct {
	ExternalUsersGroupsSource ResourceNameT `json:"externalUsersGroupsSource,omitempty" yaml:"externalUsersGroupsSource,omitempty"`
	ExternalUser              ResourceNameT `json:"externalUser,omitempty" yaml:"externalUser,omitempty"`

	ExternalGroups []ResourceNameT `json:"externalGroups,omitempty" yaml:"externalGroups,omitempty"`
}

type ExternalUser2ExternalGroupListRelationsV1 struct {
	Metadata MetadataV1                                    `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Data     ExternalUser2ExternalGroupListRelationsDataV1 `json:"data,omitempty" yaml:"data,omitempty"`
}

func NewExternalUser2ExternalGroupListRelationsV1(name ResourceNameT) *ExternalUser2ExternalGroupListRelationsV1 {
	return &ExternalUser2ExternalGroupListRelationsV1{
		Metadata: MetadataV1{
			APIVersion: APIVersionV1Value,
			Name:       name,
			Kind:       ExternalGroup2ExternalUserListV1RelationsKind,
		},
		Data: ExternalUser2ExternalGroupListRelationsDataV1{
			ExternalGroups: make([]ResourceNameT, 0),
		},
	}
}
