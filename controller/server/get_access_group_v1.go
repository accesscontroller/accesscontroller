package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func createGetAccessGroupV1(stor AccessGroupV1Getter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Access Group Name.
		name, err := parseAccessGroupName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get data.
		group, err := stor.GetAccessGroupV1Context(c.Request().Context(), name)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, group)
	}
}
