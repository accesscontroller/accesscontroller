package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createDeleteAccessGroupV1(stor AccessGroupV1Deleter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get group to delete
		var group = &storage.AccessGroupV1{}
		if err := c.Bind(group); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Delete
		if err := stor.DeleteAccessGroupV1Context(
			c.Request().Context(), group); err != nil {
			return parseError(err)
		}

		return c.NoContent(http.StatusNoContent)
	}
}
