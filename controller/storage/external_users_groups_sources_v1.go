package storage

import (
	"fmt"
)

const (
	ExternalUsersGroupsSourceKind       ResourceKindT = "ExternalUsersGroupsSource"
	ExternalUsersGroupsSourceFilterKind ResourceKindT = "ExternalUsersGroupsSourceFilter"
)

// ExternalUsersGroupsSourceV1 - one source of External Users and External Groups.
type ExternalUsersGroupsSourceV1 struct {
	Metadata MetadataV1                      `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Data     ExternalUsersGroupsSourceDataV1 `json:"data,omitempty" yaml:"data,omitempty"`
}

func (eugsv1 *ExternalUsersGroupsSourceV1) String() string {
	return fmt.Sprintf("%+v", *eugsv1)
}

// NewExternalUsersGroupsSourceV1 - creates a new ExternalUsersGroupsSourceV1.
func NewExternalUsersGroupsSourceV1(name ResourceNameT) *ExternalUsersGroupsSourceV1 {
	return &ExternalUsersGroupsSourceV1{
		Metadata: NewMetadataV1(ExternalUsersGroupsSourceKind, name, nil),
	}
}

// ExternalUsersGroupsSourceDataV1 - settings of one source.
type ExternalUsersGroupsSourceDataV1 struct {
	PollIntervalSec int      `json:"pollIntervalSec,omitempty" yaml:"pollIntervalSec,omitempty"`
	GroupsToMonitor []string `json:"groupsToMonitor,omitempty" yaml:"groupsToMonitor,omitempty"`

	GetMode              ExternalUsersGroupsSourceGetModeT `faker:"oneof: poll, passive wait" json:"getMode,omitempty" yaml:"getMode,omitempty"`
	Type                 ExternalUsersGroupsSourceTypeT    `faker:"oneof: LDAP, NOT_LDAP" json:"type,omitempty" yaml:"type,omitempty"`
	UsersSourceSettings  map[string]string                 `json:"usersSourceSettings,omitempty" yaml:"usersSourceSettings,omitempty"`
	GroupsSourceSettings map[string]string                 `json:"groupsSourceSettings,omitempty" yaml:"groupsSourceSettings,omitempty"`
}

func (v *ExternalUsersGroupsSourceDataV1) String() string {
	return fmt.Sprintf("%+v", *v)
}

// ExternalUsersGroupsSourceFilterV1 - filter data for getting ExternalUsersGroupsSources.
type ExternalUsersGroupsSourceFilterV1 struct {
	NameSubstringMatch ResourceNameT                     `json:"nameSubstringMatch,omitempty" yaml:"nameSubstringMatch,omitempty"`
	Name               ResourceNameT                     `json:"name,omitempty" yaml:"name,omitempty"`
	GetMode            ExternalUsersGroupsSourceGetModeT `json:"getMode,omitempty" yaml:"getMode,omitempty"`
	Type               ExternalUsersGroupsSourceTypeT    `json:"type,omitempty" yaml:"type,omitempty"`
}

func (eugsfv1 *ExternalUsersGroupsSourceFilterV1) String() string {
	return fmt.Sprintf("%+v", *eugsfv1)
}
