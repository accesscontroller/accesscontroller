package storage

const (
	AccessGroup2ExternalGroupListV1RelationsKind ResourceKindT = "AccessGroup2ExternalGroupListRelations"
)

type AccessGroup2ExternalGroupListRelationsDataV1 struct {
	AccessGroup ResourceNameT                     `json:"accessGroup,omitempty" yaml:"accessGroup,omitempty"`
	Relations   map[ResourceNameT][]ResourceNameT `json:"relations,omitempty" yaml:"relations,omitempty"`
}

type AccessGroup2ExternalGroupListRelationsV1 struct {
	Metadata MetadataV1                                   `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Data     AccessGroup2ExternalGroupListRelationsDataV1 `json:"data,omitempty" yaml:"data,omitempty"`
}

func NewAccessGroup2ExternalGroupListV1Relations(name ResourceNameT) *AccessGroup2ExternalGroupListRelationsV1 {
	return &AccessGroup2ExternalGroupListRelationsV1{
		Metadata: MetadataV1{
			APIVersion: APIVersionV1Value,
			Name:       name,
			Kind:       AccessGroup2ExternalGroupListV1RelationsKind,
		},
		Data: AccessGroup2ExternalGroupListRelationsDataV1{
			Relations: make(map[ResourceNameT][]ResourceNameT),
		},
	}
}
