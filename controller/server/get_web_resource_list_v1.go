package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createGetWebResourceListV1(stor WebResourceV1sGetter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get Web Resource Category.
		category := parseWebResourceCategoryName(c)

		// Get filter.
		var flt []storage.WebResourceFilterV1

		if err := c.Bind(&flt); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get filtered resources.
		resources, err := stor.GetWebResourceV1sContext(c.Request().Context(), &category, flt)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, resources)
	}
}
