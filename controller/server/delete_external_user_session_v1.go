package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createDeleteExternalUserSessionV1(stor ExternalUserSessionV1Closer) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse params.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Get Sessions source name.
		sourceName, err := parseExternalSessionsSourceName(c)
		if err != nil {
			return err
		}

		// Get Session name.
		name, err := parseExternalUserSessionName(c)
		if err != nil {
			return err
		}

		// Get payload.
		var session = &storage.ExternalUserSessionV1{}
		if err := c.Bind(session); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check source name match.
		if session.Metadata.ExternalSource.SourceName != sourceName {
			return NewErrDifferentSources(sourceName,
				session.Metadata.ExternalSource.SourceName, session).HTTPErr()
		}

		// Check name match.
		if name != session.Metadata.Name {
			return NewErrNamesDoNotMatch(name, session.Metadata.Name, session).HTTPErr()
		}

		// Create.
		closedSession, err := stor.CloseExternalUserSessionV1Context(
			c.Request().Context(), sourceName, session, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusNoContent, closedSession)
	}
}
