package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime/pprof"
	"text/tabwriter"

	"gitlab.com/accesscontroller/accesscontroller/controller/server"
	"gitlab.com/accesscontroller/sqlstorage3"
)

func main() {
	// Parse Flags config.
	flagsConfig := parseFlag()

	// Parse Env config.
	serverConfig, err := parseEnv(flagsConfig.insecureAPI)
	if err != nil {
		log.Fatalln(err)
	}

	// Enable pprof.
	if flagsConfig.cpuProfileFile != "" {
		sigInt := make(chan os.Signal, 1)

		signal.Notify(sigInt, os.Interrupt)

		f, err := os.Create(flagsConfig.cpuProfileFile)
		if err != nil {
			log.Fatal(err)
		}

		log.Printf("Starting CPU profile to %q", flagsConfig.cpuProfileFile)
		pprof.StartCPUProfile(f)

		go func() {
			// Wait for stop.
			<-sigInt

			log.Printf("Finishing CPU profile to %q", flagsConfig.cpuProfileFile)
			pprof.StopCPUProfile()
			os.Exit(0)
		}()
	}

	// Connect DB.
	storage, err := sqlstorage3.New(serverConfig.DBLogin, serverConfig.DBPassword,
		serverConfig.DBHost, serverConfig.DBName)
	if err != nil {
		log.Fatalln(err)
	}

	// Create Server.
	config, err := server.NewDefaultConfiguration()
	if err != nil {
		log.Fatalln(err)
	}

	if flagsConfig.insecureAPI {
		config.SkipRequestValidation = true
		config.SkipResponseValidation = true
		config.DisableRBAC = flagsConfig.insecureAPI
	}

	eR, err := server.New(storage, config)
	if err != nil {
		log.Fatalln(err)
	}

	// Print existing Routes.
	wrtr := tabwriter.NewWriter(os.Stdout, 0, 4, 2, ' ', 0)
	for _, r := range eR.GetRoutes() {
		fmt.Fprintf(wrtr, "%s:\t%s\n", r.Method, r.Path)
	}

	wrtr.Flush()

	if flagsConfig.insecureAPI {
		log.Fatalln(eR.StartNoAuth(serverConfig.Bind))
	} else {
		log.Fatalln(eR.StartTLS(
			serverConfig.ClientCACertPath,
			serverConfig.ServerCACertPath,
			serverConfig.ServerCertPath,
			serverConfig.ServerKeyPath,
			serverConfig.Bind))
	}
}
