package storage

import (
	"fmt"
)

const (
	ExternalUserKind       = "ExternalUser"
	ExternalUserFilterKind = "ExternalUserFilter"
)

// ExternalUserV1 - one User imported from ExternalUsersGroupsSourceV1.
type ExternalUserV1 struct {
	Metadata MetadataV1         `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Data     ExternalUserDataV1 `json:"data,omitempty" yaml:"data,omitempty"`
}

func (euv1 *ExternalUserV1) String() string {
	return fmt.Sprintf("%+v", *euv1)
}

// NewExternalUserV1 - creates a new ExternalUserV1.
func NewExternalUserV1(name ResourceNameT, usersSource ResourceNameT) *ExternalUserV1 {
	return &ExternalUserV1{
		Metadata: NewMetadataV1(ExternalUserKind, name,
			NewExternalSourceInfo(usersSource, ExternalUsersGroupsSourceKind)),
	}
}

// GetMetadata - returns copy of ExternalUserV1 MetadataV1
func (euv1 *ExternalUserV1) GetMetadata() MetadataV1 {
	return euv1.Metadata.Copy()
}

// ExternalUserDataV1 - contains fields for ExternalUserV1.
type ExternalUserDataV1 struct{}

func (v *ExternalUserDataV1) String() string {
	return fmt.Sprintf("%+v", *v)
}

// ExternalUserFilterV1 - fields of one Filter expression.
// Set fields are processed using logical AND.
type ExternalUserFilterV1 struct {
	NameSubstringMatch string        `json:"nameSubstringMatch,omitempty" yaml:"nameSubstringMatch,omitempty"`
	Name               ResourceNameT `json:"name,omitempty" yaml:"name,omitempty"`
}

func (eUFV1 *ExternalUserFilterV1) String() string {
	return fmt.Sprintf("%+v", *eUFV1)
}
