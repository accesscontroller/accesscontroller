package validator

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
)

type buffedWriter struct {
	buff             *bytes.Buffer
	origWriter       http.ResponseWriter
	header           http.Header
	isHeaderSet      bool
	headerStatusCode int
}

func newBufferedWriter(w http.ResponseWriter) *buffedWriter {

	return &buffedWriter{
		header:     make(http.Header),
		buff:       new(bytes.Buffer),
		origWriter: w,
	}
}

func (w *buffedWriter) Write(b []byte) (int, error) {
	if !w.isHeaderSet {
		w.WriteHeader(http.StatusOK)
	}

	return w.buff.Write(b)
}

func (w *buffedWriter) Header() http.Header {
	return w.header
}

func (w *buffedWriter) WriteHeader(code int) {
	if code < 100 || code > 999 {
		panic(fmt.Errorf("Invalid HTTP Status code: %d", code))
	}

	if !w.isHeaderSet {
		w.headerStatusCode = code
		w.isHeaderSet = true
	}
}

func (w *buffedWriter) GetCopyReader() io.Reader {
	return bytes.NewReader(w.buff.Bytes())
}

func (w *buffedWriter) FlushToOriginal() error {

	// Not very optimized way
	// But using hijacking makes things more difficult
	// to implements in part of headers

	//Headers
	oh := w.origWriter.Header()
	for k, v := range w.header {
		for i := range v {
			oh.Add(k, v[i])
		}
	}

	// Status
	w.origWriter.WriteHeader(w.headerStatusCode)

	// Body
	_, err := w.buff.WriteTo(w.origWriter)
	if err != nil {
		return err
	}

	return nil
}

func (w *buffedWriter) Reset() {
	w.buff.Reset()
	w.isHeaderSet = false
}

func (w *buffedWriter) GetOriginalWriter() http.ResponseWriter {
	return w.origWriter
}

func (w *buffedWriter) GetString() string {
	return w.buff.String()
}
