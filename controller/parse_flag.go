package main

import (
	"flag"
)

type flags struct {
	cpuProfileFile string
	insecureAPI    bool
}

func parseFlag() flags {
	var (
		cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")
		insecure   = flag.Bool("insecure-api", false, "if set true then API server disables all Client Certificate checks")
	)

	flag.Parse()

	return flags{
		cpuProfileFile: *cpuprofile,
		insecureAPI:    *insecure,
	}
}
