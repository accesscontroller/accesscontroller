package server

import (
	"fmt"
	"strings"

	"github.com/casbin/casbin/v2"
	defaultrolemanager "github.com/casbin/casbin/v2/rbac/default-role-manager"
	"github.com/casbin/casbin/v2/util"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/pkg/errors"
)

var (
	ErrNoTLSConnection = errors.New("connection without TLS")
	ErrNoPeerCert      = errors.New("peer does not offer client certificates")
	ErrAddingPolicy    = errors.New("can not add Policy")
)

// RBACConfig Casbin RBAC middleware configuration.
type RBACConfig struct {
	// Skipper defines a function to skip middleware.
	Skipper middleware.Skipper

	// Enforcer CasbinAuth main rule. Required.
	Enforcer *casbin.CachedEnforcer
}

// NewDefaultRBACConfig creates a new RBACConfig with default settings.
func NewDefaultRBACConfig(accessPrefixPath, casbinPolicyPath, casbinModelPath string) (RBACConfig, error) {
	const logPrefix = "can not create DefaultRBACConfig: %w"

	enforcer, err := initDefaultCasbinEnforcer(accessPrefixPath, casbinPolicyPath, casbinModelPath)
	if err != nil {
		return RBACConfig{}, fmt.Errorf(logPrefix, err)
	}

	return RBACConfig{
		Skipper:  defaultSkipper,
		Enforcer: enforcer,
	}, nil
}

// checkPermission performs request permission check.
func (cfg *RBACConfig) checkPermission(c echo.Context) error {
	const logPrefix = "can not check access permission for PeerCert %q to %q with method %s: %w"

	if !c.IsTLS() {
		return echo.ErrForbidden.SetInternal(
			fmt.Errorf("can not get User Permissions: %w", ErrNoTLSConnection))
	}

	var (
		req = c.Request()

		peerCerts    = req.TLS.PeerCertificates
		accessObject = req.URL.Path
		action       = req.Method
	)

	if len(peerCerts) == 0 {
		return echo.ErrForbidden.SetInternal(
			fmt.Errorf(logPrefix, "", accessObject, action, ErrNoPeerCert))
	}

	// Check for access
	for _, cert := range peerCerts {
		for _, certOU := range cert.Subject.OrganizationalUnit {
			allowed, err := cfg.Enforcer.Enforce(certOU, accessObject, action)
			if err != nil {
				return echo.ErrInternalServerError.SetInternal(fmt.Errorf(logPrefix,
					cert.Subject.String(), accessObject, action, err))
			}

			if allowed {
				return nil
			}
		}
	}

	// Build error report.
	var certsInfo strings.Builder

	for _, crt := range peerCerts {
		if _, err := certsInfo.WriteString(fmt.Sprintf("Subject=%q", crt.Subject.String())); err != nil {
			return echo.ErrInternalServerError.SetInternal(err)
		}
	}

	return echo.ErrForbidden.SetInternal(fmt.Errorf(
		"forbidden access for PeerCerts %s to %q with method %s",
		certsInfo.String(), accessObject, action))
}

// GetMiddleware returns a middleware for echo.
func (cfg *RBACConfig) GetMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			// Skip if required
			if cfg.Skipper(c) {
				return next(c)
			}

			// Check access
			if err := cfg.checkPermission(c); err != nil {
				return err
			}

			return next(c)
		}
	}
}

// initDefaultCasbinEnforcer initializes a new Casbin enforcer.
func initDefaultCasbinEnforcer(subjectPrefixPath, casbinPolicyPath,
	casbinModelPath string) (*casbin.CachedEnforcer, error) {
	const logPrefix = "can not init Casbin CachedEnforcer: %w"
	// Init Casbin
	enforcer, err := casbin.NewCachedEnforcer(casbinModelPath, casbinPolicyPath)
	if err != nil {
		return nil, fmt.Errorf(logPrefix, err)
	}

	/*
		Configuring URL path pattern matching as described
		at https://casbin.org/docs/en/rbac
		Default is 10 https://casbin.org/docs/en/rbac
	*/
	const maxHierarchyLevel = 10

	rm, ok := defaultrolemanager.NewRoleManager(
		maxHierarchyLevel).(*defaultrolemanager.RoleManager)
	if !ok {
		return nil, fmt.Errorf(logPrefix, errors.New(
			"can not typecast defaultrolemanager.NewRoleManager(10).(*defaultrolemanager.RoleManager)"))
	}

	rm.AddMatchingFunc("KeyMatch2", util.KeyMatch2)

	enforcer.SetRoleManager(rm)

	// LoadPolicy is required here: https://github.com/casbin/casbin/issues/352
	if err := enforcer.LoadPolicy(); err != nil {
		return nil, fmt.Errorf(logPrefix, err)
	}

	// Update Model access paths with prefix
	pathPolicies := enforcer.GetNamedGroupingPolicy("g2")

	for _, v := range pathPolicies {
		_, err := enforcer.AddNamedGroupingPolicy("g2", subjectPrefixPath+v[0], v[1])
		if err != nil {
			return nil, fmt.Errorf(logPrefix, err)
		}
	}

	return enforcer, nil
}

// defaultSkipper implements middleware.Skipper which does not skip anything.
func defaultSkipper(c echo.Context) bool {
	return false
}
