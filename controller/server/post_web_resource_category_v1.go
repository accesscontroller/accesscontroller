package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostWebResourceCategoryV1(stor WebResourceCategoryV1Creator) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse query parameters.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Get Name from query.
		name := parseWebResourceCategoryName(c)
		if name == "" {
			return &echo.HTTPError{
				Code:     http.StatusBadRequest,
				Internal: ErrEmptyWebResourceCategoryNamePathParam,
				Message:  "WebResourceCategory name must be given",
			}
		}

		// Load.
		var cat = &storage.WebResourceCategoryV1{}
		if err := c.Bind(cat); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check names match.
		if name != cat.Metadata.Name {
			return NewErrNamesDoNotMatch(name, cat.Metadata.Name, cat).HTTPErr()
		}

		// Create.
		newCat, err := stor.CreateWebResourceCategoryV1Context(
			c.Request().Context(), cat, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, newCat)
	}
}
