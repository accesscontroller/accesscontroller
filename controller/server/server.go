package server

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io"

	"io/ioutil"
	"sort"

	"github.com/labstack/echo-contrib/jaegertracing"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/pkg/errors"
	validator "gitlab.com/accesscontroller/accesscontroller/controller/middleware/openapi_validator"
)

const (
	V1Suffix string = "/v1"

	externalUsersGroupsSourceNamePathParam string = "external_groups_source_name"
	accessGroupNamePathParam               string = "access_group_name"
	externalUserNamePathParam              string = "external_user_login"
	externalGroupNamePathParam             string = "external_group_name"
	externalUserSessionNamePathParam       string = "external_user_session_name"
	webResourceCategoryNamePathParam       string = "web_resource_category_name"
	webResourceNamePathParam               string = "web_resource_name"
	externalSessionsSourceNamePathParam    string = "external_sessions_source_name"
)

var (
	ErrEmptyExternalUsersGroupsSourceNamePathParam = errors.New(
		"empty value given for ExternalUsersGroupsSourceNamePathParam")
	ErrEmptyAccessGroupNamePathParam            = errors.New("empty value given for AccessGroupNamePathParam")
	ErrEmptyExternalUserNamePathParam           = errors.New("empty value given for ExternalUserNamePathParam")
	ErrEmptyExternalGroupNamePathParam          = errors.New("empty value given for ExternalGroupNamePathParam")
	ErrEmptyExternalUserSessionNamePathParam    = errors.New("empty value given for ExternalUserSessionNamePathParam")
	ErrEmptyWebResourceCategoryNamePathParam    = errors.New("empty value given for WebResourceCategoryNamePathParam")
	ErrEmptyWebResourceNamePathParam            = errors.New("empty value given for WebResourceNamePathParam")
	ErrEmptySystemAccountNamePathParam          = errors.New("empty value given for SystemAccountNamePathParam")
	ErrEmptyExternalSessionsSourceNamePathParam = errors.New(
		"empty value given for ExternalSessionsSourceNamePathParam")
	ErrEmptySystemAccountSessionNamePathParam = errors.New("empty value given for SystemAccountSessionNamePathParam")
)

// Route contains one Server route in a human-readable form.
type Route struct {
	Name   string
	Method string
	Path   string
}

// Server implements http.Handler.
type Server struct {
	e *echo.Echo

	jaegerCloser io.Closer
}

// GetRoutes returns human-readable Server routes.
func (s *Server) GetRoutes() []*Route {
	var (
		rts = s.e.Routes()

		result = make([]*Route, len(rts))
	)

	for i, r := range rts {
		result[i] = &Route{
			Method: r.Method,
			Path:   r.Path,
			Name:   r.Name,
		}
	}

	sort.Slice(result, func(i, j int) bool {
		return result[i].Path < result[j].Path
	})

	return result
}

// StartTLS http.Handler implementation.
func (s *Server) StartTLS(clientCACertPath, serverCACertPath,
	serverCertFile, serverKeyfile, address string) (err error) {
	// Close Jaeger.
	if s.jaegerCloser != nil {
		defer s.jaegerCloser.Close()
	}

	// Load CA certs.
	tlsConfig, err := loadCerts(clientCACertPath, serverCACertPath,
		serverCertFile, serverKeyfile)
	if err != nil {
		return fmt.Errorf("can not load TLS CA certs: %w", err)
	}

	s.e.TLSServer.TLSConfig = tlsConfig

	s.e.TLSServer.Addr = address

	// Enable HTTP/2
	s.e.TLSServer.TLSConfig.NextProtos = append(s.e.TLSServer.TLSConfig.NextProtos, "h2")

	return s.e.StartServer(s.e.TLSServer)
}

func (s *Server) StartNoAuth(address string) error {
	// Close Jaeger.
	if s.jaegerCloser != nil {
		defer s.jaegerCloser.Close()
	}

	return s.e.Start(address)
}

// NewDefault initializes Server with default configuration.
func NewDefault(stor Storage) (*Server, error) {
	sc, err := NewDefaultConfiguration()
	if err != nil {
		return nil, fmt.Errorf("can not NewDefaultConfiguration: %w", err)
	}

	return New(stor, sc)
}

// New initializes Server with custom configuration.
func New(stor Storage, sc Configuration) (*Server, error) {
	// Check storage
	if stor == nil {
		return nil, fmt.Errorf("nil Storage given: %w", ErrNilStorage)
	}

	e, jCloser, err := initServer(stor, sc)
	if err != nil {
		return nil, fmt.Errorf("can not init Server: %w", err)
	}

	return &Server{e: e, jaegerCloser: jCloser}, nil
}

// initServer initializes *echo.Echo with route handlers and middleware.
func initServer(stor Storage, sc Configuration) (_ *echo.Echo, jaegerCloser io.Closer, _ error) { // nolint: funlen
	eR := echo.New()

	// Midlewares.
	eR.Use(middleware.Logger())
	eR.Use(middleware.Recover())

	// Jaeger.
	jaegerCloser = jaegertracing.New(eR, nil)

	// RBAC.
	if !sc.DisableRBAC {
		eR.Use(sc._RBACConfig.GetMiddleware())
	}

	// OpenAPI Validator.
	if !sc.SkipRequestValidation || !sc.SkipResponseValidation {
		valMw, err := validator.New(sc._APISpec, !sc.SkipRequestValidation, !sc.SkipResponseValidation)
		if err != nil {
			return nil, nil, err
		}

		eR.Use(valMw)
	}

	// Use Custom Binder because DefaultBinder fails if there are any query params.
	eR.Binder = &CustomBinder{}

	// Agent from Controller Server pool.
	// Get Users + Sessions if requested.
	eR.GET(sc._Routes.externalUserListV1Path, createGetExternalUserListV1(stor))
	eR.GET(sc._Routes.externalUserV1Path, createGetExternalUserV1(stor))
	eR.POST(sc._Routes.externalUserListV1Path, createPostExternalUserListV1(stor))
	eR.POST(sc._Routes.externalUserV1Path, createPostExternalUserV1(stor))
	eR.PATCH(sc._Routes.externalUserV1Path, createPatchExternalUserV1(stor))
	eR.DELETE(sc._Routes.externalUserV1Path, createDeleteExternalUserV1(stor))
	eR.DELETE(sc._Routes.externalUserListV1Path, createDeleteExternalUserListV1(stor))
	eR.GET(sc._Routes.externalUserV1ExternalGroupListV1Path, createGetExternalUserExternalGroupListV1(stor))
	eR.PUT(sc._Routes.externalUserV1ExternalGroupListV1Path, createPutExternalUserExternalGroupListV1(stor))
	eR.DELETE(sc._Routes.externalUserV1ExternalGroupListV1Path, createDeleteExternalUserExternalGroupListV1(stor))

	// External Groups + Users in them if requested.
	eR.GET(sc._Routes.externalGroupListV1Path, createGetExternalGroupListV1(stor))
	eR.GET(sc._Routes.externalGroupV1Path, createGetExternalGroupV1(stor))
	eR.POST(sc._Routes.externalGroupListV1Path, createPostExternalGroupListV1(stor))
	eR.POST(sc._Routes.externalGroupV1Path, createPostExternalGroupV1(stor))
	eR.PATCH(sc._Routes.externalGroupListV1Path, createPatchExternalGroupListV1(stor))
	eR.PATCH(sc._Routes.externalGroupV1Path, createPatchExternalGroupV1(stor))
	eR.DELETE(sc._Routes.externalGroupListV1Path, createDeleteExternalGroupListV1(stor))
	eR.DELETE(sc._Routes.externalGroupV1Path, createDeleteExternalGroupV1(stor))
	eR.GET(sc._Routes.externalGroupExternalUserListV1Path, createGetExternalGroupExternalUserListV1(stor))
	eR.PUT(sc._Routes.externalGroupExternalUserListV1Path, createPutExternalGroupExternalUserListV1(stor))
	eR.DELETE(sc._Routes.externalGroupExternalUserListV1Path, createDeleteExternalGroupExternalUserListV1(stor))

	// External Sessions.
	eR.GET(sc._Routes.externalUserSessionListV1Path, createGetExternalUserSessionListV1(stor))
	eR.GET(sc._Routes.externalUserSessionV1Path, createGetExternalUserSessionV1(stor))
	eR.POST(sc._Routes.externalUserSessionListV1Path, createPostExternalUserSessionListV1(stor))
	eR.POST(sc._Routes.externalUserSessionV1Path, createPostExternalUserSessionV1(stor))
	// eR.PATCH(sc._Routes.ExternalUserSessionV1Path, stubHandler) - not yet possible operation
	eR.DELETE(sc._Routes.externalUserSessionV1Path, createDeleteExternalUserSessionV1(stor))

	// Get Web Resource Categories.
	eR.GET(sc._Routes.webResourceCategoryListV1Path, createGetResourceCategoryListV1(stor))
	eR.GET(sc._Routes.webResourceCategoryV1Path, createGetWebResourceCategoryV1(stor))
	eR.POST(sc._Routes.webResourceCategoryListV1Path, createPostWebResourceCategoryListV1(stor))
	eR.POST(sc._Routes.webResourceCategoryV1Path, createPostWebResourceCategoryV1(stor))
	eR.PATCH(sc._Routes.webResourceCategoryV1Path, createPatchWebResourceCategoryV1(stor))
	eR.DELETE(sc._Routes.webResourceCategoryV1Path, createDeleteWebResourceCategoryV1(stor))

	// Web resources.
	eR.GET(sc._Routes.webResourceCategoryWebResourceListV1Path, createGetWebResourceListV1(stor))
	eR.GET(sc._Routes.webResourceCategoryWebResourceV1Path, createGetWebResourceV1(stor))
	eR.GET(sc._Routes.webResourceListV1Path, createGetWebResourceListV1(stor))
	eR.GET(sc._Routes.webResourceV1Path, createGetWebResourceV1(stor))
	eR.POST(sc._Routes.webResourceListV1Path, createPostWebResourceListV1(stor))
	eR.POST(sc._Routes.webResourceV1Path, createPostWebResourceV1(stor))
	eR.PATCH(sc._Routes.webResourceV1Path, createPatchWebResourceV1(stor))
	eR.DELETE(sc._Routes.webResourceV1Path, createDeleteWebResourceV1(stor))

	// Access rules for Groups.
	eR.GET(sc._Routes.accessGroupListV1Path, createGetAccessGroupsListV1(stor))
	eR.GET(sc._Routes.accessGroupV1Path, createGetAccessGroupV1(stor))
	eR.POST(sc._Routes.accessGroupListV1Path, createPostAccessGroupListV1(stor))
	eR.POST(sc._Routes.accessGroupV1Path, createPostAccessGroupV1(stor))
	eR.PATCH(sc._Routes.accessGroupV1Path, createPatchAccessGroupV1(stor))
	eR.DELETE(sc._Routes.accessGroupV1Path, createDeleteAccessGroupV1(stor))
	eR.GET(sc._Routes.accessGroupV1WebResourceCategoryListV1Path, createGetAccessGroupWebResourceCategoryListV1(stor))
	eR.PUT(sc._Routes.accessGroupV1WebResourceCategoryListV1Path, createPutAccessGroupWebResourceCategoryListV1(stor))
	eR.DELETE(sc._Routes.accessGroupV1WebResourceCategoryListV1Path, createDeleteAccessGroupWebResourceCategoryListV1(stor))
	eR.GET(sc._Routes.accessGroupV1ExternalGroupListV1Path, createGetAccessGroupExternalGroupListV1(stor))
	eR.PUT(sc._Routes.accessGroupV1ExternalGroupListV1Path, createPutAccessGroupExternalGroupListV1(stor))
	eR.DELETE(sc._Routes.accessGroupV1ExternalGroupListV1Path, createDeleteAccessGroupExternalGroupListV1(stor))

	// Source settings.
	// External Groups and Users sources.
	eR.GET(sc._Routes.externalUsersGroupsSourceListV1Path, createGetExternalUsersGroupsSourceListV1(stor))
	eR.GET(sc._Routes.externalUsersGroupsSourceV1Path, createGetExternalUsersGroupsSourceV1(stor))
	eR.POST(sc._Routes.externalUsersGroupsSourceListV1Path, createPostExternalUsersGroupsSourceListV1(stor))
	eR.POST(sc._Routes.externalUsersGroupsSourceV1Path, createPostExternalUsersGroupsSourceV1(stor))
	eR.PATCH(sc._Routes.externalUsersGroupsSourceV1Path, createPatchExternalUsersGroupsSourceV1(stor))
	eR.DELETE(sc._Routes.externalUsersGroupsSourceV1Path, createDeleteExternalUsersGroupsSourceV1(stor))

	// External Session sources.
	eR.GET(sc._Routes.externalSessionsSourceListV1Path, createGetExternalSessionsSourceListV1(stor))
	eR.GET(sc._Routes.externalSessionsSourceV1Path, createGetExternalSessionsSourceV1(stor))
	eR.POST(sc._Routes.externalSessionsSourceListV1Path, createPostExternalSessionsSourceListV1(stor))
	eR.POST(sc._Routes.externalSessionsSourceV1Path, createPostExternalSessionsSourceV1(stor))
	eR.PATCH(sc._Routes.externalSessionsSourceV1Path, createPatchExternalSessionsSourceV1(stor))
	eR.DELETE(sc._Routes.externalSessionsSourceV1Path, createDeleteExternalSessionsSourceV1(stor))

	// Health checks.
	eR.GET(sc._Routes.healthCheckPath, createHealthzHandler(stor))

	// List Active Routes.
	eR.GET("/", createGetServerPaths(eR.Routes()))

	return eR, jaegerCloser, nil
}

// loadCerts loads certs from files and configures tls.Config for client certificate verification.
func loadCerts(clientCAPath, serverCAPath, serverCertFile, serverKeyfile string) (*tls.Config, error) {
	const logPrefix = "can not load TLS certificate %q: %w"

	serverCACert, err := ioutil.ReadFile(serverCAPath)
	if err != nil {
		return nil, fmt.Errorf(logPrefix, serverCAPath, err)
	}

	serverCert, err := ioutil.ReadFile(serverCertFile)
	if err != nil {
		return nil, fmt.Errorf(logPrefix, serverCertFile, err)
	}

	serverKey, err := ioutil.ReadFile(serverKeyfile)
	if err != nil {
		return nil, fmt.Errorf(logPrefix, serverKeyfile, err)
	}

	serverCAPool := x509.NewCertPool()
	if !serverCAPool.AppendCertsFromPEM(serverCACert) {
		return nil, fmt.Errorf(logPrefix, serverCAPath, ErrLoadTLSCert)
	}

	serverKeyPair, err := tls.X509KeyPair(serverCert, serverKey)
	if err != nil {
		return nil, fmt.Errorf("can not load TLS KeyPair from %q:%q: %w",
			serverCertFile, serverKeyfile, err)
	}

	tlsConf := tls.Config{
		RootCAs: serverCAPool,
	}

	tlsConf.Certificates = append(tlsConf.Certificates, serverKeyPair)

	clientCACert, err := ioutil.ReadFile(clientCAPath)
	if err != nil {
		return nil, fmt.Errorf(logPrefix, clientCAPath, err)
	}

	clientCAPool := x509.NewCertPool()
	if !clientCAPool.AppendCertsFromPEM(clientCACert) {
		return nil, fmt.Errorf(logPrefix, clientCAPath, ErrLoadTLSCert)
	}

	tlsConf.ClientAuth = tls.RequireAndVerifyClientCert
	tlsConf.ClientCAs = clientCAPool

	return &tlsConf, nil
}
