package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createDeleteWebResourceCategoryV1(stor WebResourceCategoryV1Deleter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Name.
		name := parseWebResourceCategoryName(c)
		if name == "" {
			return &echo.HTTPError{
				Code:     http.StatusBadRequest,
				Internal: ErrEmptyWebResourceCategoryNamePathParam,
				Message:  "WebResourceCategory name must be given",
			}
		}

		// Load.
		var cat = &storage.WebResourceCategoryV1{}
		if err := c.Bind(cat); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check names.
		if name != cat.Metadata.Name {
			return NewErrNamesDoNotMatch(name, cat.Metadata.Name, cat).HTTPErr()
		}

		// Delete.
		if err := stor.DeleteWebResourceCategoryV1Context(c.Request().Context(), cat); err != nil {
			return parseError(err)
		}

		return c.NoContent(http.StatusNoContent)
	}
}
