module gitlab.com/accesscontroller/accesscontroller/statsserver/server

go 1.12

require (
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	gitlab.com/accesscontroller/accesscontroller/controller/storage v0.0.0-20190708053500-80d1a754d76d // indirect
)
