package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createDeleteExternalGroupExternalUserListV1(stor ExternalGroup2ExternalUserV1RelationUnBinder) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get Source Name.
		source, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return err
		}

		// Group Name.
		group, err := parseExternalGroupName(c)
		if err != nil {
			return err
		}

		// Parse Payload.
		var relations storage.ExternalGroup2ExternalUserListRelationsV1

		if err := c.Bind(&relations); err != nil {
			return err
		}

		// Check values.
		if source != relations.Data.ExternalUsersGroupsSource {
			return &echo.HTTPError{
				Code:    http.StatusBadRequest,
				Message: "Path ExternalUsersGroupsSource is not equal to ExternalGroup2ExternalUserListRelationsV1#Data#ExternalUsersGroupsSource",
			}
		}

		if group != relations.Data.ExternalGroup {
			return &echo.HTTPError{
				Code:    http.StatusBadRequest,
				Message: "Path ExternalGroup is not equal to ExternalGroup2ExternalUserListRelationsV1#Data#ExternalGroup",
			}
		}

		if err := stor.UnBindExternalGroup2ExternalUserV1sContext(c.Request().Context(), &relations); err != nil {
			return parseError(err)
		}

		return c.NoContent(http.StatusNoContent)
	}
}
