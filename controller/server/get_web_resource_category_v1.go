package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func createGetWebResourceCategoryV1(stor WebResourceCategoryV1Getter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Name.
		name := parseWebResourceCategoryName(c)
		if name == "" {
			return &echo.HTTPError{
				Code:     http.StatusBadRequest,
				Internal: ErrEmptyWebResourceCategoryNamePathParam,
				Message:  "WebResourceCategory name must be given",
			}
		}

		// Create.
		cat, err := stor.GetWebResourceCategoryV1Context(c.Request().Context(), name)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, cat)
	}
}
