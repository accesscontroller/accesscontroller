package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func createGetExternalGroupExternalUserListV1(stor ExternalGroup2ExternalUserV1RelationGetter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get Source Name.
		source, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return err
		}

		// Group Name.
		group, err := parseExternalGroupName(c)
		if err != nil {
			return err
		}

		// Load data.
		users, err := stor.GetExternalGroupExternalUserV1sContext(c.Request().Context(), source, group)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, users)
	}
}
