package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPatchWebResourceCategoryV1(stor WebResourceCategoryV1Updater) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get load Web Resources param.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Get name.
		name := parseWebResourceCategoryName(c)
		if name == "" {
			return &echo.HTTPError{
				Code:     http.StatusBadRequest,
				Internal: ErrEmptyWebResourceCategoryNamePathParam,
				Message:  "WebResourceCategory name must be given",
			}
		}

		// Load update.
		var upd = &storage.WebResourceCategoryV1{}
		if err := c.Bind(upd); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Patch.
		patched, err := stor.UpdateWebResourceCategoryV1Context(
			c.Request().Context(), name, upd, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, patched)
	}
}
