package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostExternalGroupListV1(stor ExternalGroupV1sCreator) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Params.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Parse Groups source name.
		sourceName, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Parse Group List.
		var eGList []storage.ExternalGroupV1

		if err := c.Bind(&eGList); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check groups source.
		for i := range eGList {
			if sourceName != eGList[i].Metadata.ExternalSource.SourceName {
				return NewErrDifferentSources(sourceName,
					eGList[i].Metadata.ExternalSource.SourceName,
					eGList[i].Metadata)
			}
		}

		// Create.
		list, err := stor.CreateExternalGroupV1sContext(
			c.Request().Context(), eGList, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, list)
	}
}
