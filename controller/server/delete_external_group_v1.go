package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createDeleteExternalGroupV1(stor ExternalGroupV1Deleter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get Source name.
		sourceName, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get name.
		name, err := parseExternalGroupName(c)
		if err != nil {
			return err
		}

		// Parse Group.
		extGroup := &storage.ExternalGroupV1{}
		if err := c.Bind(extGroup); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check source name match.
		if sourceName != extGroup.Metadata.ExternalSource.SourceName {
			return NewErrDifferentSources(sourceName,
				extGroup.Metadata.ExternalSource.SourceName, extGroup).HTTPErr()
		}

		// Check name match.
		if name != extGroup.Metadata.Name {
			return NewErrNamesDoNotMatch(name, extGroup.Metadata.Name, extGroup).HTTPErr()
		}

		// Get Data.
		if err := stor.DeleteExternalGroupV1Context(c.Request().Context(), extGroup); err != nil {
			return parseError(err)
		}

		return c.NoContent(http.StatusNoContent)
	}
}
