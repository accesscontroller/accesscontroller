package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"
)

/*
Jaeger Tracing could be configured using Environment Variables:
JAEGER_SERVICE_NAME 	The service name
JAEGER_AGENT_HOST 	The hostname for communicating with agent via UDP
JAEGER_AGENT_PORT 	The port for communicating with agent via UDP
JAEGER_ENDPOINT 	The HTTP endpoint for sending spans directly to a collector, i.e. http://jaeger-collector:14268/api/traces
JAEGER_USER 	Username to send as part of “Basic” authentication to the collector endpoint
JAEGER_PASSWORD 	Password to send as part of “Basic” authentication to the collector endpoint
JAEGER_REPORTER_LOG_SPANS 	Whether the reporter should also log the spans
JAEGER_REPORTER_MAX_QUEUE_SIZE 	The reporter’s maximum queue size
JAEGER_REPORTER_FLUSH_INTERVAL 	The reporter’s flush interval, with units, e.g. “500ms” or “2s” ([valid units][timeunits])
JAEGER_SAMPLER_TYPE 	The sampler type
JAEGER_SAMPLER_PARAM 	The sampler parameter (number)
JAEGER_SAMPLER_MANAGER_HOST_PORT 	The HTTP endpoint when using the remote sampler, i.e. http://jaeger-agent:5778/sampling
JAEGER_SAMPLER_MAX_OPERATIONS 	The maximum number of operations that the sampler will keep track of
JAEGER_SAMPLER_REFRESH_INTERVAL 	How often the remotely controlled sampler will poll jaeger-agent for the appropriate sampling strategy, with units, e.g. “1m” or “30s” ([valid units][timeunits])
JAEGER_TAGS 	A comma separated list of name = value tracer level tags, which get added to all reported spans. The value can also refer to an environment variable using the format ${envVarName:default}, where the :default is optional, and identifies a value to be used if the environment variable cannot be found
JAEGER_DISABLED 	Whether the tracer is disabled or not. If true, the default opentracing.NoopTracer is used.
JAEGER_RPC_METRICS 	Whether to store RPC metrics.
*/

const (
	controllerEnvVarPrefix string = "ACCESS_CONTROLLER_"
	bcryptEnvVarName       string = "BCRYPT_DIFFICULTY"
	bindEnvVarName         string = "BIND"

	clientCAPathEnvVarName     string = "CLIENT_CA"
	serverCACertPathEnvVarName string = "SERVER_CA"
	serverCertPathEnvVarName   string = "SERVER_CERT"
	serverKeyPathEnvVarName    string = "SERVER_KEY"

	databaseLogin    string = "DB_LOGIN"
	databasePassword string = "DB_PASSWORD"
	databaseHostPort string = "DB_HOSTPORT"
	databaseName     string = "DB_NAME"
)

const (
	bcryptDefaultDifficulty = 10
)

var (
	ErrNoEnvVar      = errors.New("required Environment Variable is not set")
	ErrUnsupportedDB = errors.New("unsupported database")
)

type config struct {
	BCryptDifficulty int
	Bind             string
	APISpecPath      string

	ClientCACertPath string
	ServerCACertPath string
	ServerCertPath   string
	ServerKeyPath    string

	DBLogin    string
	DBPassword string
	DBHost     string
	DBName     string
}

func parseEnv(insecureAPI bool) (config, error) { // nolint: gunlen
	var resConf = config{
		BCryptDifficulty: bcryptDefaultDifficulty,
		Bind:             ":8080",
	}

	if !insecureAPI {
		r, err := parseCerts(&resConf)
		if err != nil {
			return resConf, err
		}

		resConf = *r
	}

	// Bcrypt strength
	rawBCrypt, ok := os.LookupEnv(controllerEnvVarPrefix + bcryptEnvVarName)
	if ok {
		v, err := strconv.ParseInt(rawBCrypt, 10, 32)
		if err != nil {
			return resConf, fmt.Errorf("can not parse bcrypt password hash strength: %w", err)
		}

		resConf.BCryptDifficulty = int(v)
	}

	// Bind Socket
	rawBind, ok := os.LookupEnv(controllerEnvVarPrefix + bindEnvVarName)
	if ok {
		resConf.Bind = rawBind
	}

	// Database.
	resConf.DBName, ok = os.LookupEnv(controllerEnvVarPrefix + databaseName)
	if !ok {
		return resConf, fmt.Errorf("no DB_LOGIN set")
	}

	resConf.DBHost, ok = os.LookupEnv(controllerEnvVarPrefix + databaseHostPort)
	if !ok {
		return resConf, fmt.Errorf("no DB_HOSTPORT set")
	}

	resConf.DBLogin, ok = os.LookupEnv(controllerEnvVarPrefix + databaseLogin)
	if !ok {
		return resConf, fmt.Errorf("no DB_LOGIN set")
	}

	resConf.DBPassword, ok = os.LookupEnv(controllerEnvVarPrefix + databasePassword)
	if !ok {
		return resConf, fmt.Errorf("no DB_PASSWORD set")
	}

	return resConf, nil
}

func parseCerts(resConf *config) (*config, error) {
	// Certificates paths
	rawClientCA, ok := os.LookupEnv(controllerEnvVarPrefix + clientCAPathEnvVarName)
	if !ok {
		return nil, fmt.Errorf("client CA certificate path is not set in %q: %w",
			controllerEnvVarPrefix+clientCAPathEnvVarName, ErrNoEnvVar)
	}

	resConf.ClientCACertPath = rawClientCA

	rawServerCA, ok := os.LookupEnv(controllerEnvVarPrefix + serverCACertPathEnvVarName)
	if !ok {
		return nil, fmt.Errorf("server CA certificate path is not set in %q: %w",
			controllerEnvVarPrefix+serverCACertPathEnvVarName, ErrNoEnvVar)
	}

	resConf.ServerCACertPath = rawServerCA

	rawServerCert, ok := os.LookupEnv(controllerEnvVarPrefix + serverCertPathEnvVarName)
	if !ok {
		return nil, fmt.Errorf("server certificate path is not set in %q: %w",
			controllerEnvVarPrefix+serverCertPathEnvVarName, ErrNoEnvVar)
	}

	resConf.ServerCertPath = rawServerCert

	rawServerKey, ok := os.LookupEnv(controllerEnvVarPrefix + serverKeyPathEnvVarName)
	if !ok {
		return nil, fmt.Errorf("server key path is not set in %q: %w",
			controllerEnvVarPrefix+serverKeyPathEnvVarName, ErrNoEnvVar)
	}

	resConf.ServerKeyPath = rawServerKey

	return resConf, nil
}
