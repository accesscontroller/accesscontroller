package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createDeleteExternalUserV1(stor ExternalUserV1Deleter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get source name.
		sourceName, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get Name.
		name, err := parseExternalUserName(c)
		if err != nil {
			return err
		}

		// Parse.
		var user = &storage.ExternalUserV1{}
		if err := c.Bind(user); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check Source Name match.
		if sourceName != user.Metadata.ExternalSource.SourceName {
			return NewErrDifferentSources(sourceName,
				user.Metadata.ExternalSource.SourceName, user).HTTPErr()
		}

		// Check name.
		if name != user.Metadata.Name {
			return NewErrNamesDoNotMatch(name, user.Metadata.Name, user).HTTPErr()
		}

		// Create.
		err = stor.DeleteExternalUserV1Context(
			c.Request().Context(), user)
		if err != nil {
			return parseError(err)
		}

		return c.NoContent(http.StatusNoContent)
	}
}
