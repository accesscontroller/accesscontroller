package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createGetAccessGroupsListV1(stor AccessGroupV1sGetter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse filter.
		var flt []storage.AccessGroupFilterV1

		if err := c.Bind(&flt); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get data.
		list, err := stor.GetAccessGroupV1sContext(c.Request().Context(), flt)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, list)
	}
}
