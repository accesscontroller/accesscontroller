package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func createGetAccessGroupExternalGroupListV1(stor AccessGroup2ExternalGroupV1RelationGetter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse Access Group.
		accessGroup, err := parseAccessGroupName(c)
		if err != nil {
			return err
		}

		// Load.
		relations, err := stor.GetAccessGroupExternalGroupV1sContext(c.Request().Context(), accessGroup)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, relations)
	}
}
