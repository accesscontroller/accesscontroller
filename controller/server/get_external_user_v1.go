package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func createGetExternalUserV1(stor ExternalUserV1Getter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// User name.
		eun, err := parseExternalUserName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// User's source name.
		eusn, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Data.
		user, err := stor.GetExternalUserV1Context(c.Request().Context(),
			eun, eusn)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, user)
	}
}
