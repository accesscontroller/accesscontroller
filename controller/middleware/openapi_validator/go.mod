module gitlab.com/accesscontroller/accesscontroller/controller/middleware/openapi_validator

go 1.12

require (
	github.com/getkin/kin-openapi v0.2.0
	github.com/labstack/echo/v4 v4.1.14
	github.com/stretchr/objx v0.2.0 // indirect
	golang.org/x/tools v0.0.0-20190608022120-eacb66d2a7c3 // indirect
)
