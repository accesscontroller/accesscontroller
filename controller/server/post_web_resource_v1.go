package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostWebResourceV1(stor WebResourceV1Creator) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse query parameters.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Get name from path.
		name, err := parseWebResourceName(c)
		if err != nil {
			return err
		}

		// Get payload.
		var resource = &storage.WebResourceV1{}
		if err := c.Bind(resource); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check names match.
		if name != resource.Metadata.Name {
			return NewErrNamesDoNotMatch(name, resource.Metadata.Name, resource).HTTPErr()
		}

		// Create.
		newResource, err := stor.CreateWebResourceV1Context(
			c.Request().Context(), resource, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, newResource)
	}
}
