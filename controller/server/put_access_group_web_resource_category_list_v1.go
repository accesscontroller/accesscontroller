package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPutAccessGroupWebResourceCategoryListV1(stor AccessGroup2WebResourceCategoryV1RelationBinder) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Access Group name.
		accessGroup, err := parseAccessGroupName(c)
		if err != nil {
			return err
		}

		var relations storage.AccessGroup2WebResourceCategoryListRelationsV1
		if err := c.Bind(&relations); err != nil {
			return err
		}

		if accessGroup != relations.Data.AccessGroup {
			return &echo.HTTPError{
				Code:    http.StatusBadRequest,
				Message: "Path AccessGroup name is not equal to AccessGroup2WebResourceCategoryListV1Relations#AccessGroup",
			}
		}

		if err := stor.BindAccessGroup2WebResourceCategoryV1sContext(c.Request().Context(),
			accessGroup, relations.Data.Categories); err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, nil)
	}
}
