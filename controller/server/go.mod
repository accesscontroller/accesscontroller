module gitlab.com/accesscontroller/accesscontroller/controller/server

go 1.14

require (
	github.com/HdrHistogram/hdrhistogram-go v1.0.0 // indirect
	github.com/casbin/casbin/v2 v2.19.4
	github.com/getkin/kin-openapi v0.33.0 // indirect
	github.com/go-openapi/swag v0.19.12 // indirect
	github.com/labstack/echo-contrib v0.9.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/opentracing/opentracing-go v1.2.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/uber/jaeger-client-go v2.25.0+incompatible // indirect
	github.com/uber/jaeger-lib v2.4.0+incompatible // indirect
	gitlab.com/accesscontroller/accesscontroller/controller/middleware/openapi_validator v0.0.0-20201030165051-959181aa057a
	gitlab.com/accesscontroller/accesscontroller/controller/storage v0.0.0-20201030165051-959181aa057a
	go.uber.org/atomic v1.7.0 // indirect
	golang.org/x/crypto v0.0.0-20201208171446-5f87f3452ae9 // indirect
	golang.org/x/net v0.0.0-20201209123823-ac852fbbde11 // indirect
	golang.org/x/sys v0.0.0-20201211090839-8ad439b19e0f // indirect
	golang.org/x/text v0.3.4 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
