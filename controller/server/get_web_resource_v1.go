package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func createGetWebResourceV1(stor WebResourceV1Getter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get WebResourceCategory.
		category := parseWebResourceCategoryName(c)

		// Get name.
		resourceName, err := parseWebResourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Create.
		resource, err := stor.GetWebResourceV1Context(c.Request().Context(), &category, resourceName)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, resource)
	}
}
