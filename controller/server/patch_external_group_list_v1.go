package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPatchExternalGroupListV1(stor ExternalGroupV1sUpdater) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse query params.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Parse Group List.
		var extGroupList []storage.ExternalGroupV1

		if err := c.Bind(&extGroupList); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get Data.
		list, err := stor.UpdateExternalGroupV1sContext(
			c.Request().Context(), extGroupList,
			params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, list)
	}
}
