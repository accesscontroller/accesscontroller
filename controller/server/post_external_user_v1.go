package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostExternalUserV1(stor ExternalUserV1Creator) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse query parameters.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Get External source name.
		sourceName, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get User name.
		name, err := parseExternalUserName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Parse.
		var user = &storage.ExternalUserV1{}
		if err := c.Bind(user); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check names match.
		if name != user.Metadata.Name {
			return NewErrNamesDoNotMatch(name, user.Metadata.Name, user).HTTPErr()
		}

		// Check External Source name match.
		if sourceName != user.Metadata.ExternalSource.SourceName {
			return NewErrDifferentSources(sourceName,
				user.Metadata.ExternalSource.SourceName, user).HTTPErr()
		}

		// Create.
		user, err = stor.CreateExternalUserV1Context(
			c.Request().Context(), user, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, user)
	}
}
