package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPatchExternalSessionsSourceV1(stor ExternalSessionsSourceV1Updater) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get Params.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Get Name.
		name, err := parseExternalSessionsSourceName(c)
		if err != nil {
			return err
		}

		// Get sessions source to update.
		var ess = &storage.ExternalSessionsSourceV1{}
		if err := c.Bind(ess); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Update.
		updated, err := stor.UpdateExternalSessionsSourceV1Context(
			c.Request().Context(), name, ess, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, updated)
	}
}
