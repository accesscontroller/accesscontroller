package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPatchExternalGroupV1(stor ExternalGroupV1Updater) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse query params.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Parse path params.
		gn, err := parseExternalGroupName(c)
		if err != nil {
			return err
		}

		// Parse a Group.
		extGroup := &storage.ExternalGroupV1{}
		if err := c.Bind(extGroup); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get Data.
		list, err := stor.UpdateExternalGroupV1Context(
			c.Request().Context(), gn, extGroup, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, list)
	}
}
