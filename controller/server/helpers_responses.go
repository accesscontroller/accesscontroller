package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func parseError(err error) error {
	cause := errors.Cause(err)

	switch tCause := cause.(type) {
	case *storage.ErrETagDoesNotMatch:
		return &echo.HTTPError{
			Message:  tCause,
			Internal: tCause,
			Code:     http.StatusGone,
		}

	case *storage.ErrForbiddenUpdateReadOnlyField:
		rErr := echo.ErrBadRequest.SetInternal(tCause)
		rErr.Message = tCause

		return rErr

	case *storage.ErrResourceAlreadyExists:
		return &echo.HTTPError{
			Code:     http.StatusConflict,
			Message:  tCause,
			Internal: tCause,
		}

	case *storage.ErrResourceNotFound:
		rErr := echo.ErrNotFound.SetInternal(tCause)
		rErr.Message = tCause

		return rErr

	case *storage.ErrRelatedResourcesNotFound:
		rErr := echo.ErrNotFound.SetInternal(tCause)
		rErr.Message = tCause

		return rErr

	default:
		rErr := echo.ErrInternalServerError.SetInternal(err)
		rErr.Message = err

		return rErr
	}
}
