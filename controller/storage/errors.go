package storage

import (
	"encoding/json"
	"fmt"
)

type ErrResourceAlreadyExists struct {
	conflictingResources []interface{}

	err error
}

func NewErrResourceAlreadyExists(conflictingResources []interface{}, internalError error) *ErrResourceAlreadyExists {
	return &ErrResourceAlreadyExists{
		conflictingResources: conflictingResources,
		err:                  internalError,
	}
}

func (err *ErrResourceAlreadyExists) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}{
		"error_kind":     "ErrResourceAlreadyExists",
		"conflicts":      err.conflictingResources,
		"internal_error": err.err.Error(),
	})
}

func (err *ErrResourceAlreadyExists) Error() string {
	bts, e := json.Marshal(err)
	if e != nil {
		return fmt.Sprintf("Marshal Error: %s", e.Error())
	}

	return string(bts)
}

type ErrETagDoesNotMatch struct {
	expected ETagT
	given    ETagT
}

func NewErrETagDoesNotMatch(expected, given ETagT) *ErrETagDoesNotMatch {
	return &ErrETagDoesNotMatch{
		expected: expected,
		given:    given,
	}
}

func (err *ErrETagDoesNotMatch) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}{
		"error_kind": "ErrETagDoesNotMatch",
		"expected":   err.expected,
		"given":      err.given,
	})
}

func (err *ErrETagDoesNotMatch) Error() string {
	return fmt.Sprintf("given ETag %v is not equal to ETag from storage %v",
		err.given, err.expected)
}

type ErrResourceNotFound struct {
	metadata MetadataV1
}

func NewErrResourceNotFound(md *MetadataV1) *ErrResourceNotFound {
	return &ErrResourceNotFound{
		metadata: md.Copy(),
	}
}

func (err *ErrResourceNotFound) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}{
		"error_kind": "ErrResourceNotFound",
		"metadata":   err.metadata,
	})
}

func (err *ErrResourceNotFound) Error() string {
	return fmt.Sprintf("requested resource %v was not found", err.metadata)
}

type ErrForbiddenUpdateReadOnlyField struct {
	metadata           MetadataV1
	forbiddenFieldName string
}

func NewErrForbiddenUpdateReadOnlyField(
	metadata MetadataV1,
	forbiddenFieldName string) *ErrForbiddenUpdateReadOnlyField {
	return &ErrForbiddenUpdateReadOnlyField{
		metadata:           metadata,
		forbiddenFieldName: forbiddenFieldName,
	}
}

func (err *ErrForbiddenUpdateReadOnlyField) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}{
		"error_kind":      "ErrForbiddenUpdateReadOnlyField",
		"metadata":        err.metadata,
		"forbidden_field": err.forbiddenFieldName,
	})
}

func (err *ErrForbiddenUpdateReadOnlyField) Error() string {
	return fmt.Sprintf("can not update a read only field %s for resource %v",
		err.forbiddenFieldName, err.metadata)
}

type ErrRelatedResourcesNotFound struct {
	apiVersion APIVersionT
	kind       ResourceKindT
	names      []ResourceNameT
	resources  []interface{}
}

func (err *ErrRelatedResourcesNotFound) Error() string {
	if err.resources == nil {
		return fmt.Sprintf(
			"related resources { ApiVersion: %s, Kind: %s, Names: %v } were not found",
			err.apiVersion, err.kind, err.names)
	}

	return fmt.Sprintf("related resources %+v were not found", err.resources)
}

func (err *ErrRelatedResourcesNotFound) MarshalJSON() ([]byte, error) {
	if err.resources != nil {
		return json.Marshal(map[string]interface{}{
			"error_kind": "ErrRelatedResourcesNotFound",
			"resources":  err.resources,
		})
	}

	return json.Marshal(map[string]interface{}{
		"error_kind":  "ErrRelatedResourcesNotFound",
		"api_version": err.apiVersion,
		"kind":        err.kind,
		"names":       err.names,
	})
}

// NewErrRelatedResourcesNotFound - creates a new ErrRelatedResourcesNotFound
// if resources []interface{} is set then other arguments are not used
func NewErrRelatedResourcesNotFound(apiVersion APIVersionT, kind ResourceKindT,
	resourceNames []ResourceNameT, resources []interface{}) *ErrRelatedResourcesNotFound {
	return &ErrRelatedResourcesNotFound{
		apiVersion: apiVersion,
		kind:       kind,
		names:      resourceNames,
		resources:  resources,
	}
}

type ErrIncorrectResourceKindGiven struct {
	givenKind     ResourceKindT
	expectedKind  ResourceKindT
	givenMetadata MetadataV1
}

func NewErrIncorrectResourceKindGiven(givenKind, expectedKind ResourceKindT,
	givenMeta MetadataV1) *ErrIncorrectResourceKindGiven {
	return &ErrIncorrectResourceKindGiven{
		givenKind:     givenKind,
		expectedKind:  expectedKind,
		givenMetadata: givenMeta,
	}
}

func (err *ErrIncorrectResourceKindGiven) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}{
		"error_kind":     "ErrIncorrectResourceKindGiven",
		"given_kind":     err.givenKind,
		"expected_kind":  err.expectedKind,
		"given_metadata": err.givenMetadata,
	})
}

func (err *ErrIncorrectResourceKindGiven) Error() string {
	return fmt.Sprintf(
		"Incorrect resource kind %s which expected %s for object %+v",
		err.givenKind, err.expectedKind, err.givenMetadata)
}

type ErrIncorrectExternalSource struct {
	expectedSource ResourceNameT

	givenMD MetadataV1
}

func NewErrIncorrectExternalSource(source ResourceNameT, md MetadataV1) *ErrIncorrectExternalSource {
	return &ErrIncorrectExternalSource{
		expectedSource: source,
		givenMD:        md,
	}
}

func (err *ErrIncorrectExternalSource) Error() string {
	return fmt.Sprintf("Expected ExrernalSource is %q, given MedatadaV1 is %+v",
		err.expectedSource, err.givenMD)
}

func (err *ErrIncorrectExternalSource) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}{
		"error_kind":      "ErrIncorrectExternalSource",
		"given_metadata":  err.givenMD,
		"expected_source": err.expectedSource,
	})
}
