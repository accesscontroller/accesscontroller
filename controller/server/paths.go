package server

import (
	"net/http"
	"sort"

	"github.com/labstack/echo/v4"
)

func createGetServerPaths(routes []*echo.Route) echo.HandlerFunc {
	var resp = make([]*echo.Route, 0, len(routes))

	for _, v := range routes {
		resp = append(resp, &echo.Route{
			Method: v.Method,
			Path:   v.Path,
			Name:   v.Name,
		})
	}

	sort.Slice(resp, func(i, j int) bool {
		return resp[i].Path < resp[j].Path
	})

	return func(c echo.Context) error {
		return c.JSON(http.StatusOK, resp)
	}
}
