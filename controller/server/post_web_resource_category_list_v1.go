package server

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostWebResourceCategoryListV1(stor WebResourceCategoryV1sCreator) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse query parameters.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Load.
		var catList []storage.WebResourceCategoryV1

		if err := c.Bind(&catList); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Create.
		newList, err := stor.CreateWebResourceCategoryV1sContext(c.Request().Context(), catList, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, newList)
	}
}
