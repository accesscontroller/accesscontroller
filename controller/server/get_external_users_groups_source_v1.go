package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func createGetExternalUsersGroupsSourceV1(stor ExternalUsersGroupsSourceV1Getter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get Source name.
		name, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get groups source.
		src, err := stor.GetExternalUsersGroupsSourceV1Context(
			c.Request().Context(), name)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, src)
	}
}
