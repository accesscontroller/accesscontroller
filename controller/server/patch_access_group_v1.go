package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPatchAccessGroupV1(stor AccessGroupV1Updater) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Get Group Name To Update from path.
		groupName, err := parseAccessGroupName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get query parameters.
		params, err := parseQueryParameters(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Read Patch.
		patch := &storage.AccessGroupV1{}
		if err := c.Bind(patch); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Update.
		updated, err := stor.UpdateAccessGroupV1Context(
			c.Request().Context(), groupName, patch, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, updated)
	}
}
