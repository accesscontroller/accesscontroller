package server

import (
	"fmt"
	"io/ioutil"
	"os"
)

const (
	APIDefaultPrefix        string = ""
	APISpecDefaultPath      string = "server/apispec/openapi.yaml"
	CasbinModelDefaultPath  string = "server/access_policy/model.conf"
	CasbinPolicyDefaultPath string = "server/access_policy/policy.csv"
)

// Configuration contains options required to run server.
type Configuration struct {
	DisableRBAC            bool
	SkipRequestValidation  bool
	SkipResponseValidation bool

	_APISpec      []byte
	_RBACConfig   RBACConfig
	APIPrefixPath string
	_Routes       URLPaths

	ClientCACertPath string
	ServerCACertPath string
	ServerCertFile   string
	ServerKeyfile    string
}

// NewDefaultConfiguration initializes Configuration with default
// settings.
func NewDefaultConfiguration() (Configuration, error) {
	return NewConfiguration(APISpecDefaultPath, APIDefaultPrefix,
		CasbinModelDefaultPath, CasbinPolicyDefaultPath)
}

// NewConfiguration initializes Configuration with user-provided settings.
func NewConfiguration(_APISpecPath, _APIPrefixPath,
	casbinModelPath, casbinPolicyPath string) (Configuration, error) {
	const logPrefix = "can not init Configuration: %w"

	// Load API Spec
	apiSpec, err := readFile(_APISpecPath)
	if err != nil {
		return Configuration{}, fmt.Errorf(logPrefix, err)
	}

	// Init Casbin
	rbac, err := NewDefaultRBACConfig(_APIPrefixPath+V1Suffix, casbinPolicyPath, casbinModelPath)
	if err != nil {
		return Configuration{}, fmt.Errorf(logPrefix, err)
	}

	return Configuration{
		_APISpec:      apiSpec,
		APIPrefixPath: _APIPrefixPath,
		_RBACConfig:   rbac,
		_Routes:       newDefaultURLPaths(_APIPrefixPath),
	}, nil
}

// readFile reads and returns given file.
func readFile(path string) ([]byte, error) {
	specFile, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("can not open file %q: %w", path, err)
	}

	defer func() {
		if err := specFile.Close(); err != nil {
			panic(err)
		}
	}()

	return ioutil.ReadAll(specFile)
}
