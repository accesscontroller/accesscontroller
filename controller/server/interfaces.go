package server

import (
	"context"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// AccessGroupV1Deleter - Deletes AccessGroup
type AccessGroupV1Deleter interface {
	DeleteAccessGroupV1Context(ctx context.Context, group *storage.AccessGroupV1) error
}

// AccessGroupV1Getter -  Returns one Access group by its name
type AccessGroupV1Getter interface {
	GetAccessGroupV1Context(ctx context.Context, name storage.ResourceNameT) (*storage.AccessGroupV1, error)
}

// AccessGroupV1sGetter - Returns AccessGroupList
type AccessGroupV1sGetter interface {
	GetAccessGroupV1sContext(ctx context.Context, f []storage.AccessGroupFilterV1) ([]storage.AccessGroupV1, error)
}

// AccessGroupV1sCreator - Creates multiple AccessGroupRules from AccessGroupList
type AccessGroupV1sCreator interface {
	CreateAccessGroupV1sContext(ctx context.Context,
		groupList []storage.AccessGroupV1, getCreated bool) ([]storage.AccessGroupV1, error)
}

// AccessGroupV1Creator - Creates a new AccessGroup
type AccessGroupV1Creator interface {
	CreateAccessGroupV1Context(ctx context.Context,
		group *storage.AccessGroupV1, getCreated bool) (*storage.AccessGroupV1, error)
}

// AccessGroupV1Updater - modifies AccessGroup
type AccessGroupV1Updater interface {
	UpdateAccessGroupV1Context(ctx context.Context, groupNameToUpdate storage.ResourceNameT,
		group *storage.AccessGroupV1, getUpdated bool) (*storage.AccessGroupV1, error)
}

type ExternalGroupV1sGetter interface {
	GetExternalGroupV1sContext(ctx context.Context, filter []storage.ExternalGroupFilterV1,
		externalUsersGroupsSourceName storage.ResourceNameT) ([]storage.ExternalGroupV1, error)
}

type ExternalGroupV1Getter interface {
	GetExternalGroupV1Context(ctx context.Context, name storage.ResourceNameT,
		sourceName storage.ResourceNameT) (*storage.ExternalGroupV1, error)
}

type ExternalGroupV1sCreator interface {
	CreateExternalGroupV1sContext(ctx context.Context,
		groups []storage.ExternalGroupV1, getCreated bool) ([]storage.ExternalGroupV1, error)
}

type ExternalGroupV1Creator interface {
	CreateExternalGroupV1Context(ctx context.Context,
		group *storage.ExternalGroupV1, getCreated bool) (*storage.ExternalGroupV1, error)
}

type ExternalGroupV1sUpdater interface {
	UpdateExternalGroupV1sContext(ctx context.Context, list []storage.ExternalGroupV1,
		getUpdated bool) ([]storage.ExternalGroupV1, error)
}

type ExternalGroupV1Updater interface {
	UpdateExternalGroupV1Context(ctx context.Context, oldName storage.ResourceNameT,
		group *storage.ExternalGroupV1, getUpdated bool) (*storage.ExternalGroupV1, error)
}

type ExternalGroupV1sDeleter interface {
	DeleteExternalGroupV1sContext(ctx context.Context, groups []storage.ExternalGroupV1) error
}

type ExternalGroupV1Deleter interface {
	DeleteExternalGroupV1Context(ctx context.Context, group *storage.ExternalGroupV1) error
}

type ExternalSessionsSourceV1Getter interface {
	GetExternalSessionsSourceV1Context(ctx context.Context, name storage.ResourceNameT,
	) (*storage.ExternalSessionsSourceV1, error)
}

type ExternalSessionsSourceV1sGetter interface {
	GetExternalSessionsSourceV1sContext(ctx context.Context,
		filter []storage.ExternalSessionsSourceFilterV1) ([]storage.ExternalSessionsSourceV1, error)
}

type ExternalSessionsSourceV1Creator interface {
	CreateExternalSessionsSourceV1Context(ctx context.Context,
		source *storage.ExternalSessionsSourceV1, getCreated bool) (*storage.ExternalSessionsSourceV1, error)
}

type ExternalSessionsSourceV1sCreator interface {
	CreateExternalSessionsSourceV1sContext(ctx context.Context,
		source []storage.ExternalSessionsSourceV1, getCreated bool) ([]storage.ExternalSessionsSourceV1, error)
}

type ExternalSessionsSourceV1Updater interface {
	UpdateExternalSessionsSourceV1Context(ctx context.Context, name storage.ResourceNameT,
		source *storage.ExternalSessionsSourceV1, getUpdated bool) (*storage.ExternalSessionsSourceV1, error)
}

type ExternalSessionsSourceV1Deleter interface {
	DeleteExternalSessionsSourceV1Context(ctx context.Context,
		source *storage.ExternalSessionsSourceV1) error
}

type ExternalUserSessionV1Getter interface {
	GetExternalUserSessionV1Context(ctx context.Context,
		externalSessionsSourceName storage.ResourceNameT,
		name storage.ResourceNameT) (*storage.ExternalUserSessionV1, error)
}

type ExternalUserSessionV1sGetter interface {
	GetExternalUserSessionV1sContext(ctx context.Context,
		sessionsSourceName storage.ResourceNameT,
		filter []storage.ExternalUserSessionFilterV1) ([]storage.ExternalUserSessionV1, error)
}

type ExternalUserSessionV1Opener interface {
	OpenExternalUserSessionV1Context(ctx context.Context,
		session *storage.ExternalUserSessionV1, getOpened bool) (*storage.ExternalUserSessionV1, error)
}

type ExternalUserSessionV1sOpener interface {
	OpenExternalUserSessionV1sContext(ctx context.Context,
		sessions []storage.ExternalUserSessionV1, getOpened bool) ([]storage.ExternalUserSessionV1, error)
}

type ExternalUserSessionV1Closer interface {
	CloseExternalUserSessionV1Context(ctx context.Context,
		externalSessionsSourceName storage.ResourceNameT,
		session *storage.ExternalUserSessionV1, getClosed bool) (*storage.ExternalUserSessionV1, error)
}

type ExternalUsersGroupsSourceV1sGetter interface {
	GetExternalUsersGroupsSourceV1sContext(ctx context.Context,
		filter []storage.ExternalUsersGroupsSourceFilterV1) ([]storage.ExternalUsersGroupsSourceV1, error)
}

type ExternalUsersGroupsSourceV1Getter interface {
	GetExternalUsersGroupsSourceV1Context(ctx context.Context,
		name storage.ResourceNameT) (*storage.ExternalUsersGroupsSourceV1, error)
}

type ExternalUsersGroupsSourceV1sCreator interface {
	CreateExternalUsersGroupsSourceV1sContext(ctx context.Context,
		source []storage.ExternalUsersGroupsSourceV1, getCreated bool) ([]storage.ExternalUsersGroupsSourceV1, error)
}

type ExternalUsersGroupsSourceV1Creator interface {
	CreateExternalUsersGroupsSourceV1Context(ctx context.Context,
		source *storage.ExternalUsersGroupsSourceV1, getCreated bool) (*storage.ExternalUsersGroupsSourceV1, error)
}

type ExternalUsersGroupsSourceV1Updater interface {
	UpdateExternalUsersGroupsSourceV1Context(ctx context.Context, name storage.ResourceNameT,
		patch *storage.ExternalUsersGroupsSourceV1, getUpdated bool) (*storage.ExternalUsersGroupsSourceV1, error)
}

type ExternalUsersGroupsSourceV1Deleter interface {
	DeleteExternalUsersGroupsSourceV1Context(ctx context.Context,
		source *storage.ExternalUsersGroupsSourceV1) error
}

type ExternalUserV1sGetter interface {
	GetExternalUserV1sContext(ctx context.Context, filter []storage.ExternalUserFilterV1,
		sourceName storage.ResourceNameT) ([]storage.ExternalUserV1, error)
}

type ExternalUserV1Getter interface {
	GetExternalUserV1Context(ctx context.Context, name storage.ResourceNameT,
		source storage.ResourceNameT) (*storage.ExternalUserV1, error)
}

type ExternalUserV1sCreator interface {
	CreateExternalUserV1sContext(ctx context.Context,
		users []storage.ExternalUserV1, getCreated bool) ([]storage.ExternalUserV1, error)
}

type ExternalUserV1Creator interface {
	CreateExternalUserV1Context(ctx context.Context, users *storage.ExternalUserV1, getCreated bool) (*storage.ExternalUserV1, error)
}

type ExternalUserV1Updater interface {
	UpdateExternalUserV1Context(ctx context.Context, oldName storage.ResourceNameT,
		user *storage.ExternalUserV1, getUpdated bool) (*storage.ExternalUserV1, error)
}

type ExternalUserV1Deleter interface {
	DeleteExternalUserV1Context(ctx context.Context, users *storage.ExternalUserV1) error
}

type ExternalUserV1sDeleter interface {
	DeleteExternalUserV1sContext(ctx context.Context, list []storage.ExternalUserV1) error
}

type WebResourceCategoryV1sGetter interface {
	GetWebResourceCategoryV1sContext(ctx context.Context,
		filter []storage.WebResourceCategoryFilterV1) ([]storage.WebResourceCategoryV1, error)
}

type WebResourceCategoryV1Getter interface {
	GetWebResourceCategoryV1Context(ctx context.Context, name storage.ResourceNameT) (*storage.WebResourceCategoryV1, error)
}

type WebResourceCategoryV1sCreator interface {
	CreateWebResourceCategoryV1sContext(ctx context.Context,
		categories []storage.WebResourceCategoryV1, getCreated bool) ([]storage.WebResourceCategoryV1, error)
}

type WebResourceCategoryV1Creator interface {
	CreateWebResourceCategoryV1Context(ctx context.Context,
		category *storage.WebResourceCategoryV1, getCreated bool) (*storage.WebResourceCategoryV1, error)
}

type WebResourceCategoryV1Updater interface {
	UpdateWebResourceCategoryV1Context(ctx context.Context, name storage.ResourceNameT,
		category *storage.WebResourceCategoryV1, getUpdated bool) (*storage.WebResourceCategoryV1, error)
}

type WebResourceCategoryV1Deleter interface {
	DeleteWebResourceCategoryV1Context(ctx context.Context,
		category *storage.WebResourceCategoryV1) error
}

type WebResourceV1sGetter interface {
	GetWebResourceV1sContext(ctx context.Context, category *storage.ResourceNameT,
		filter []storage.WebResourceFilterV1) ([]storage.WebResourceV1, error)
}

type WebResourceV1Getter interface {
	GetWebResourceV1Context(ctx context.Context, category *storage.ResourceNameT,
		name storage.ResourceNameT) (*storage.WebResourceV1, error)
}

type WebResourceV1sCreator interface {
	CreateWebResourceV1sContext(ctx context.Context,
		resources []storage.WebResourceV1, getCreated bool) ([]storage.WebResourceV1, error)
}

type WebResourceV1Creator interface {
	CreateWebResourceV1Context(ctx context.Context,
		resource *storage.WebResourceV1, getCreated bool) (*storage.WebResourceV1, error)
}

type WebResourceV1Updater interface {
	UpdateWebResourceV1Context(ctx context.Context, name storage.ResourceNameT,
		resource *storage.WebResourceV1, getUpdated bool) (*storage.WebResourceV1, error)
}

type WebResourceV1Deleter interface {
	DeleteWebResourceV1Context(ctx context.Context, resource *storage.WebResourceV1) error
}

type HealthChecker interface {
	CheckHealthContext(ctx context.Context) error
}

type AccessGroup2WebResourceCategoryV1RelationGetter interface {
	GetAccessGroupWebResourceCategoryV1sContext(_ context.Context,
		accessGroup storage.ResourceNameT) ([]storage.ResourceNameT, error)
}

type AccessGroup2WebResourceCategoryV1RelationBinder interface {
	BindAccessGroup2WebResourceCategoryV1sContext(_ context.Context,
		accessGroup storage.ResourceNameT, webResourceCategories []storage.ResourceNameT) error
}

type AccessGroup2WebResourceCategoryV1RelationUnBinder interface {
	UnBindAccessGroup2WebResourceCategoryV1sContext(_ context.Context,
		accessGroup storage.ResourceNameT, webResourceCategories []storage.ResourceNameT) error
}

type AccessGroup2WebResourceCategoryV1RelationManager interface {
	AccessGroup2WebResourceCategoryV1RelationGetter
	AccessGroup2WebResourceCategoryV1RelationBinder
	AccessGroup2WebResourceCategoryV1RelationUnBinder
}

type AccessGroup2ExternalGroupV1RelationGetter interface {
	GetAccessGroupExternalGroupV1sContext(_ context.Context, accessGroup storage.ResourceNameT) (*storage.AccessGroup2ExternalGroupListRelationsV1, error)
}

type AccessGroup2ExternalGroupV1RelationBinder interface {
	BindAccessGroup2ExternalGroupV1sContext(_ context.Context, relations *storage.AccessGroup2ExternalGroupListRelationsV1) error
}

type AccessGroup2ExternalGroupV1RelationUnBinder interface {
	UnBindAccessGroup2ExternalGroupV1sContext(_ context.Context, relations *storage.AccessGroup2ExternalGroupListRelationsV1) error
}

type AccessGroup2ExternalGroupV1RelationManager interface {
	AccessGroup2ExternalGroupV1RelationGetter
	AccessGroup2ExternalGroupV1RelationBinder
	AccessGroup2ExternalGroupV1RelationUnBinder
}

type ExternalGroup2ExternalUserV1RelationGetter interface {
	GetExternalGroupExternalUserV1sContext(_ context.Context,
		externalGroupSource, externalGroup storage.ResourceNameT) (*storage.ExternalGroup2ExternalUserListRelationsV1, error)
}

type ExternalGroup2ExternalUserV1RelationBinder interface {
	BindExternalGroup2ExternalUserV1sContext(_ context.Context, relations *storage.ExternalGroup2ExternalUserListRelationsV1) error
}

type ExternalGroup2ExternalUserV1RelationUnBinder interface {
	UnBindExternalGroup2ExternalUserV1sContext(_ context.Context, relations *storage.ExternalGroup2ExternalUserListRelationsV1) error
}

type ExternalGroup2ExternalUserV1RelationManager interface {
	ExternalGroup2ExternalUserV1RelationGetter
	ExternalGroup2ExternalUserV1RelationBinder
	ExternalGroup2ExternalUserV1RelationUnBinder
}

type ExternalUser2ExternalGroupV1RelationGetter interface {
	GetExternalUserExternalGroupV1sContext(_ context.Context,
		externalGroupSource, externalUser storage.ResourceNameT) (*storage.ExternalUser2ExternalGroupListRelationsV1, error)
}

type ExternalUser2ExternalGroupV1RelationBinder interface {
	BindExternalUser2ExternalGroupV1sContext(_ context.Context,
		relations *storage.ExternalUser2ExternalGroupListRelationsV1) error
}

type ExternalUser2ExternalGroupV1RelationUnBinder interface {
	UnBindExternalUser2ExternalGroupV1sContext(_ context.Context,
		relations *storage.ExternalUser2ExternalGroupListRelationsV1) error
}

type ExternalUser2ExternalGroupV1RelationManager interface {
	ExternalUser2ExternalGroupV1RelationGetter
	ExternalUser2ExternalGroupV1RelationBinder
	ExternalUser2ExternalGroupV1RelationUnBinder
}

type Storage interface {
	ExternalUserV1sGetter
	ExternalUserV1Getter
	ExternalUserV1sCreator
	ExternalUserV1Creator
	ExternalUserV1Updater
	ExternalUserV1Deleter
	ExternalUserV1sDeleter

	ExternalGroupV1sGetter
	ExternalGroupV1Getter
	ExternalGroupV1sCreator
	ExternalGroupV1Creator
	ExternalGroupV1sUpdater
	ExternalGroupV1Updater
	ExternalGroupV1sDeleter
	ExternalGroupV1Deleter

	ExternalUserSessionV1sGetter
	ExternalUserSessionV1Getter
	ExternalUserSessionV1sOpener
	ExternalUserSessionV1Opener
	ExternalUserSessionV1Closer

	WebResourceCategoryV1sGetter
	WebResourceCategoryV1Getter
	WebResourceCategoryV1sCreator
	WebResourceCategoryV1Creator
	WebResourceCategoryV1Updater
	WebResourceCategoryV1Deleter

	WebResourceV1sGetter
	WebResourceV1Getter
	WebResourceV1sCreator
	WebResourceV1Creator
	WebResourceV1Updater
	WebResourceV1Deleter

	AccessGroupV1sGetter
	AccessGroupV1Getter
	AccessGroupV1sCreator
	AccessGroupV1Creator
	AccessGroupV1Updater
	AccessGroupV1Deleter

	ExternalUsersGroupsSourceV1sGetter
	ExternalUsersGroupsSourceV1Getter
	ExternalUsersGroupsSourceV1sCreator
	ExternalUsersGroupsSourceV1Creator
	ExternalUsersGroupsSourceV1Updater
	ExternalUsersGroupsSourceV1Deleter

	ExternalSessionsSourceV1sGetter
	ExternalSessionsSourceV1Getter
	ExternalSessionsSourceV1sCreator
	ExternalSessionsSourceV1Creator
	ExternalSessionsSourceV1Updater
	ExternalSessionsSourceV1Deleter

	HealthChecker

	AccessGroup2WebResourceCategoryV1RelationManager
	AccessGroup2ExternalGroupV1RelationManager
	ExternalGroup2ExternalUserV1RelationManager
	ExternalUser2ExternalGroupV1RelationManager
}
