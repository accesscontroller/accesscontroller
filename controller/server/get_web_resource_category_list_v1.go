package server

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createGetResourceCategoryListV1(stor WebResourceCategoryV1sGetter) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Bind request.
		var flt []storage.WebResourceCategoryFilterV1

		if err := c.Bind(&flt); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Get Data
		res, err := stor.GetWebResourceCategoryV1sContext(c.Request().Context(), flt)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusOK, res)
	}
}
