# Go bin path
GO_BIN := go
GO_RACE_DETECTION_FLAGS := -race
GO_RACE_DETECTION_BIN_SUFFIX := race

# Output dir path
BUILD_OUTPUT_PATH := $(CURDIR)/out

### Controller variables
CONTROLLER_OUT_DIR := controller
CONTROLLER_SRC_DIR_NAME := controller
CONTROLLER_BUILD_TAGS := -tags "sqlite3 sqlite_stat4 sqlite_foreign_keys"
# Binary out paths
_CONTROLLER_BUILD_PATH := $(BUILD_OUTPUT_PATH)/$(CONTROLLER_OUT_DIR)
_CONTROLLER_BIN_OUT := $(_CONTROLLER_BUILD_PATH)/api_server
# OpenAPI spec
_CONTROLLER_SRC_API_SPEC_PATH := server/apispec/openapi.yaml
_CONTROLLER_DST_API_SPEC_PATH := $(_CONTROLLER_BUILD_PATH)/server/apispec/openapi.yaml
# RBAC policy
_CONTROLLER_SRC_RBAC_POLICY_PATH := server/access_policy/policy.csv
_CONTROLLER_SRC_RBAC_MODEL_PATH := server/access_policy/model.conf

_CONTROLLER_DST_RBAC_DIR := $(_CONTROLLER_BUILD_PATH)/server/access_policy
_CONTROLLER_DST_RBAC_POLICY_PATH := $(_CONTROLLER_DST_RBAC_DIR)/policy.csv
_CONTROLLER_DST_RBAC_MODEL_PATH := $(_CONTROLLER_DST_RBAC_DIR)/model.conf
# PKI folder
_CONTROLLER_PKI_DIR := $(_CONTROLLER_BUILD_PATH)/keys/
# Start script
_CONTROLLER_SRC_START_SCRIPT := run.sh
_CONTROLLER_DST_START_SCRIPT := $(_CONTROLLER_BUILD_PATH)/run.sh

# SQL storage variables
SQLSTORAGE_SRC_DIR_NAME := sqlstorage
SQLSTORAGE_SRC_SQLITE3_MIGRATIONS_DIR := $(SQLSTORAGE_SRC_DIR_NAME)/migrations_sqlite3
SQLSTORAGE_DST_SQLITE3_MIGRATIONS_DIR := $(_CONTROLLER_BUILD_PATH)/sql_migrations

### MS AD Sessions Adapter variables
MS_AD_SESSIONS_ADAPTER_SRC_DIR_NAME := ms-ad-sessions-adapter
MS_AD_SESSIONS_ADAPTER_OUT_DIR := ms-ad-sessions-adapter
MS_AD_SESSIONS_ADAPTER_BUILD_FLAGS := ""
# Binary out paths
_MS_AD_SESSIONS_ADAPTER_BUILD_PATH := $(BUILD_OUTPUT_PATH)/$(MS_AD_SESSIONS_ADAPTER_OUT_DIR)
_MS_AD_SESSIONS_ADAPTER_BIN_OUT := $(_MS_AD_SESSIONS_ADAPTER_BUILD_PATH)/ms-ad-sessions-adapter.exe
_MS_AD_SESSIONS_ADAPTER_GOOS := windows
_MS_AD_SESSIONS_ADAPTER_SRC_DEFAULT_CONFIG_PATH := $(MS_AD_SESSIONS_ADAPTER_SRC_DIR_NAME)/adapter_config.yaml
_MS_AD_SESSIONS_ADAPTER_DST_DEFAULT_CONFIG_PATH := $(_MS_AD_SESSIONS_ADAPTER_BUILD_PATH)/adapter_config.yaml.example

help:
	@echo "Here will be dragons"

clean: clean-controller

build-controller: controller-output-dir copy-controller-spec copy-controller-policy \
	prepare-controller-pki-dir copy-controller-start-script copy-sqlite3-migrations
	cd $(CONTROLLER_SRC_DIR_NAME); \
	$(GO_BIN) build -o $(_CONTROLLER_BIN_OUT) \
	$(CONTROLLER_BUILD_TAGS)

build-controller-race: controller-output-dir copy-controller-spec copy-controller-policy \
	prepare-controller-pki-dir copy-controller-start-script copy-sqlite3-migrations
	cd $(CONTROLLER_SRC_DIR_NAME); \
	$(GO_BIN) build -o $(_CONTROLLER_BIN_OUT)-$(GO_RACE_DETECTION_BIN_SUFFIX) \
	$(GO_RACE_DETECTION_FLAGS) $(CONTROLLER_BUILD_TAGS)

test-controller:
	cd $(CONTROLLER_SRC_DIR_NAME); \
	$(GO_BIN) test

clean-controller:
	rm -f $(_CONTROLLER_BIN_OUT) \
		$(_CONTROLLER_BIN_OUT)-$(GO_RACE_DETECTION_BIN_SUFFIX) \
		$(_CONTROLLER_DST_API_SPEC_PATH) \
		$(_CONTROLLER_DST_RBAC_POLICY_PATH) \
		$(_CONTROLLER_DST_RBAC_MODEL_PATH)

controller-output-dir:
	mkdir -p $(_CONTROLLER_BUILD_PATH)

copy-controller-spec:
	mkdir -p `dirname $(_CONTROLLER_DST_API_SPEC_PATH)`
	cp $(CONTROLLER_SRC_DIR_NAME)/$(_CONTROLLER_SRC_API_SPEC_PATH) $(_CONTROLLER_DST_API_SPEC_PATH)

copy-controller-policy:
	mkdir -p $(_CONTROLLER_DST_RBAC_DIR)
	cp $(CONTROLLER_SRC_DIR_NAME)/$(_CONTROLLER_SRC_RBAC_POLICY_PATH) $(_CONTROLLER_DST_RBAC_POLICY_PATH)
	cp $(CONTROLLER_SRC_DIR_NAME)/$(_CONTROLLER_SRC_RBAC_MODEL_PATH) $(_CONTROLLER_DST_RBAC_MODEL_PATH)

prepare-controller-pki-dir:
	mkdir -p $(_CONTROLLER_PKI_DIR)

copy-controller-start-script:
	cp $(CONTROLLER_SRC_DIR_NAME)/$(_CONTROLLER_SRC_START_SCRIPT) $(_CONTROLLER_DST_START_SCRIPT)
	chmod +x $(_CONTROLLER_DST_START_SCRIPT)

copy-sqlite3-migrations:
	mkdir -p $(SQLSTORAGE_DST_SQLITE3_MIGRATIONS_DIR)
	cp -r $(SQLSTORAGE_SRC_SQLITE3_MIGRATIONS_DIR) $(SQLSTORAGE_DST_SQLITE3_MIGRATIONS_DIR)

ms-ad-sessions-adapter-output-dir:
	mkdir -p $(_MS_AD_SESSIONS_ADAPTER_BUILD_PATH)

ms-ad-sessions-adapter-default-config:
	cp $(_MS_AD_SESSIONS_ADAPTER_SRC_DEFAULT_CONFIG_PATH) $(_MS_AD_SESSIONS_ADAPTER_DST_DEFAULT_CONFIG_PATH)

build-ms-ad-sessions-adapter: ms-ad-sessions-adapter-output-dir ms-ad-sessions-adapter-default-config
	cd $(MS_AD_SESSIONS_ADAPTER_SRC_DIR_NAME); \
	GOOS=$(_MS_AD_SESSIONS_ADAPTER_GOOS) go build -o $(_MS_AD_SESSIONS_ADAPTER_BIN_OUT) $(MS_AD_SESSIONS_ADAPTER_BUILD_FLAGS)
