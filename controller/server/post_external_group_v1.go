package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostExternalGroupV1(stor ExternalGroupV1Creator) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parse query parameters.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Parse group name from path.
		name, err := parseExternalGroupName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Parse group source.
		sourceName, err := parseExternalUsersGroupsSourceName(c)
		if err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Parse Group.
		var extGroup = &storage.ExternalGroupV1{}
		if err := c.Bind(extGroup); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Check names match.
		if name != extGroup.Metadata.Name {
			return NewErrNamesDoNotMatch(
				name, extGroup.Metadata.Name, extGroup).HTTPErr()
		}

		// Check source names match.
		if sourceName != extGroup.Metadata.ExternalSource.SourceName {
			return NewErrDifferentSources(sourceName,
				extGroup.Metadata.ExternalSource.SourceName, extGroup).HTTPErr()
		}

		// Get Data.
		list, err := stor.CreateExternalGroupV1Context(
			c.Request().Context(), extGroup, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, list)
	}
}
