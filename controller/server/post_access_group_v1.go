package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func createPostAccessGroupV1(stor AccessGroupV1Creator) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Parameters.
		params, err := parseQueryParameters(c)
		if err != nil {
			return err
		}

		// Parse Access Group.
		payload := &storage.AccessGroupV1{}
		if err := c.Bind(payload); err != nil {
			return echo.ErrBadRequest.SetInternal(err)
		}

		// Create a new Access Group.
		created, err := stor.CreateAccessGroupV1Context(c.Request().Context(),
			payload, params.GetOperationResult)
		if err != nil {
			return parseError(err)
		}

		return c.JSON(http.StatusCreated, created)
	}
}
